﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Settings))
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.CheckCityWebsite = New System.Windows.Forms.CheckBox()
        Me.CheckCityPhone = New System.Windows.Forms.CheckBox()
        Me.CheckYouTube = New System.Windows.Forms.CheckBox()
        Me.CheckTwitter = New System.Windows.Forms.CheckBox()
        Me.CheckFacebook = New System.Windows.Forms.CheckBox()
        Me.CheckWebsite = New System.Windows.Forms.CheckBox()
        Me.Checkemail = New System.Windows.Forms.CheckBox()
        Me.CheckPhone = New System.Windows.Forms.CheckBox()
        Me.CheckTitle = New System.Windows.Forms.CheckBox()
        Me.CheckName = New System.Windows.Forms.CheckBox()
        Me.CheckGuest6 = New System.Windows.Forms.CheckBox()
        Me.CheckGuest5 = New System.Windows.Forms.CheckBox()
        Me.CheckGuest4 = New System.Windows.Forms.CheckBox()
        Me.CheckGuest3 = New System.Windows.Forms.CheckBox()
        Me.CheckGuest2 = New System.Windows.Forms.CheckBox()
        Me.CheckGuest1 = New System.Windows.Forms.CheckBox()
        Me.CheckFloor3 = New System.Windows.Forms.CheckBox()
        Me.CheckFloor2 = New System.Windows.Forms.CheckBox()
        Me.CheckFloor1 = New System.Windows.Forms.CheckBox()
        Me.CheckCamera3 = New System.Windows.Forms.CheckBox()
        Me.CheckCamera2 = New System.Windows.Forms.CheckBox()
        Me.CheckCamera1 = New System.Windows.Forms.CheckBox()
        Me.CheckAudio2 = New System.Windows.Forms.CheckBox()
        Me.CheckAudio1 = New System.Windows.Forms.CheckBox()
        Me.CheckGraphics2 = New System.Windows.Forms.CheckBox()
        Me.CheckGraphics1 = New System.Windows.Forms.CheckBox()
        Me.CheckDirector2 = New System.Windows.Forms.CheckBox()
        Me.CheckDirector1 = New System.Windows.Forms.CheckBox()
        Me.CheckHost2 = New System.Windows.Forms.CheckBox()
        Me.CheckHost1 = New System.Windows.Forms.CheckBox()
        Me.CheckProducer2 = New System.Windows.Forms.CheckBox()
        Me.CheckProducer1 = New System.Windows.Forms.CheckBox()
        Me.CheckShowTitle = New System.Windows.Forms.CheckBox()
        Me.CheckRecordDate = New System.Windows.Forms.CheckBox()
        Me.CheckShowYouTube = New System.Windows.Forms.CheckBox()
        Me.CheckShowTwitter = New System.Windows.Forms.CheckBox()
        Me.CheckShowFacebook = New System.Windows.Forms.CheckBox()
        Me.CheckShowWebsite = New System.Windows.Forms.CheckBox()
        Me.CheckShowemail = New System.Windows.Forms.CheckBox()
        Me.CheckShowPhone = New System.Windows.Forms.CheckBox()
        Me.CheckCommentLine = New System.Windows.Forms.CheckBox()
        Me.LBGuestFields = New System.Windows.Forms.Label()
        Me.LBCrewFields = New System.Windows.Forms.Label()
        Me.LBShowFields = New System.Windows.Forms.Label()
        Me.CheckOfficeemail = New System.Windows.Forms.CheckBox()
        Me.CheckOfficePhone = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'ButtonSave
        '
        Me.ButtonSave.BackColor = System.Drawing.Color.White
        Me.ButtonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.Blue
        Me.ButtonSave.Location = New System.Drawing.Point(492, 279)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(118, 23)
        Me.ButtonSave.TabIndex = 1001
        Me.ButtonSave.Text = "Save Settings"
        Me.ButtonSave.UseVisualStyleBackColor = False
        '
        'CheckCityWebsite
        '
        Me.CheckCityWebsite.AutoSize = True
        Me.CheckCityWebsite.Location = New System.Drawing.Point(101, 243)
        Me.CheckCityWebsite.Name = "CheckCityWebsite"
        Me.CheckCityWebsite.Size = New System.Drawing.Size(115, 17)
        Me.CheckCityWebsite.TabIndex = 2587
        Me.CheckCityWebsite.Text = "City/State Website"
        Me.CheckCityWebsite.UseVisualStyleBackColor = True
        '
        'CheckCityPhone
        '
        Me.CheckCityPhone.AutoSize = True
        Me.CheckCityPhone.Location = New System.Drawing.Point(101, 222)
        Me.CheckCityPhone.Name = "CheckCityPhone"
        Me.CheckCityPhone.Size = New System.Drawing.Size(107, 17)
        Me.CheckCityPhone.TabIndex = 2586
        Me.CheckCityPhone.Text = "City/State Phone"
        Me.CheckCityPhone.UseVisualStyleBackColor = True
        '
        'CheckYouTube
        '
        Me.CheckYouTube.AutoSize = True
        Me.CheckYouTube.Location = New System.Drawing.Point(101, 199)
        Me.CheckYouTube.Name = "CheckYouTube"
        Me.CheckYouTube.Size = New System.Drawing.Size(70, 17)
        Me.CheckYouTube.TabIndex = 2585
        Me.CheckYouTube.Text = "YouTube"
        Me.CheckYouTube.UseVisualStyleBackColor = True
        '
        'CheckTwitter
        '
        Me.CheckTwitter.AutoSize = True
        Me.CheckTwitter.Location = New System.Drawing.Point(101, 177)
        Me.CheckTwitter.Name = "CheckTwitter"
        Me.CheckTwitter.Size = New System.Drawing.Size(58, 17)
        Me.CheckTwitter.TabIndex = 2584
        Me.CheckTwitter.Text = "Twitter"
        Me.CheckTwitter.UseVisualStyleBackColor = True
        '
        'CheckFacebook
        '
        Me.CheckFacebook.AutoSize = True
        Me.CheckFacebook.Location = New System.Drawing.Point(101, 154)
        Me.CheckFacebook.Name = "CheckFacebook"
        Me.CheckFacebook.Size = New System.Drawing.Size(74, 17)
        Me.CheckFacebook.TabIndex = 2583
        Me.CheckFacebook.Text = "Facebook"
        Me.CheckFacebook.UseVisualStyleBackColor = True
        '
        'CheckWebsite
        '
        Me.CheckWebsite.AutoSize = True
        Me.CheckWebsite.Location = New System.Drawing.Point(101, 132)
        Me.CheckWebsite.Name = "CheckWebsite"
        Me.CheckWebsite.Size = New System.Drawing.Size(65, 17)
        Me.CheckWebsite.TabIndex = 2582
        Me.CheckWebsite.Text = "Website"
        Me.CheckWebsite.UseVisualStyleBackColor = True
        '
        'Checkemail
        '
        Me.Checkemail.AutoSize = True
        Me.Checkemail.Location = New System.Drawing.Point(101, 110)
        Me.Checkemail.Name = "Checkemail"
        Me.Checkemail.Size = New System.Drawing.Size(50, 17)
        Me.Checkemail.TabIndex = 2581
        Me.Checkemail.Text = "email"
        Me.Checkemail.UseVisualStyleBackColor = True
        '
        'CheckPhone
        '
        Me.CheckPhone.AutoSize = True
        Me.CheckPhone.Location = New System.Drawing.Point(101, 88)
        Me.CheckPhone.Name = "CheckPhone"
        Me.CheckPhone.Size = New System.Drawing.Size(57, 17)
        Me.CheckPhone.TabIndex = 2580
        Me.CheckPhone.Text = "Phone"
        Me.CheckPhone.UseVisualStyleBackColor = True
        '
        'CheckTitle
        '
        Me.CheckTitle.AutoSize = True
        Me.CheckTitle.Location = New System.Drawing.Point(101, 66)
        Me.CheckTitle.Name = "CheckTitle"
        Me.CheckTitle.Size = New System.Drawing.Size(46, 17)
        Me.CheckTitle.TabIndex = 2579
        Me.CheckTitle.Text = "Title"
        Me.CheckTitle.UseVisualStyleBackColor = True
        '
        'CheckName
        '
        Me.CheckName.AutoSize = True
        Me.CheckName.Location = New System.Drawing.Point(101, 45)
        Me.CheckName.Name = "CheckName"
        Me.CheckName.Size = New System.Drawing.Size(54, 17)
        Me.CheckName.TabIndex = 2578
        Me.CheckName.Text = "Name"
        Me.CheckName.UseVisualStyleBackColor = True
        '
        'CheckGuest6
        '
        Me.CheckGuest6.AutoSize = True
        Me.CheckGuest6.Location = New System.Drawing.Point(21, 154)
        Me.CheckGuest6.Name = "CheckGuest6"
        Me.CheckGuest6.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest6.TabIndex = 2577
        Me.CheckGuest6.Text = "Guest 6"
        Me.CheckGuest6.UseVisualStyleBackColor = True
        '
        'CheckGuest5
        '
        Me.CheckGuest5.AutoSize = True
        Me.CheckGuest5.Location = New System.Drawing.Point(21, 132)
        Me.CheckGuest5.Name = "CheckGuest5"
        Me.CheckGuest5.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest5.TabIndex = 2576
        Me.CheckGuest5.Text = "Guest 5"
        Me.CheckGuest5.UseVisualStyleBackColor = True
        '
        'CheckGuest4
        '
        Me.CheckGuest4.AutoSize = True
        Me.CheckGuest4.Location = New System.Drawing.Point(21, 110)
        Me.CheckGuest4.Name = "CheckGuest4"
        Me.CheckGuest4.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest4.TabIndex = 2575
        Me.CheckGuest4.Text = "Guest 4"
        Me.CheckGuest4.UseVisualStyleBackColor = True
        '
        'CheckGuest3
        '
        Me.CheckGuest3.AutoSize = True
        Me.CheckGuest3.Location = New System.Drawing.Point(21, 88)
        Me.CheckGuest3.Name = "CheckGuest3"
        Me.CheckGuest3.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest3.TabIndex = 2574
        Me.CheckGuest3.Text = "Guest 3"
        Me.CheckGuest3.UseVisualStyleBackColor = True
        '
        'CheckGuest2
        '
        Me.CheckGuest2.AutoSize = True
        Me.CheckGuest2.Location = New System.Drawing.Point(21, 66)
        Me.CheckGuest2.Name = "CheckGuest2"
        Me.CheckGuest2.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest2.TabIndex = 2573
        Me.CheckGuest2.Text = "Guest 2"
        Me.CheckGuest2.UseVisualStyleBackColor = True
        '
        'CheckGuest1
        '
        Me.CheckGuest1.AutoSize = True
        Me.CheckGuest1.Location = New System.Drawing.Point(21, 45)
        Me.CheckGuest1.Name = "CheckGuest1"
        Me.CheckGuest1.Size = New System.Drawing.Size(63, 17)
        Me.CheckGuest1.TabIndex = 2572
        Me.CheckGuest1.Text = "Guest 1"
        Me.CheckGuest1.UseVisualStyleBackColor = True
        '
        'CheckFloor3
        '
        Me.CheckFloor3.AutoSize = True
        Me.CheckFloor3.Location = New System.Drawing.Point(364, 199)
        Me.CheckFloor3.Name = "CheckFloor3"
        Me.CheckFloor3.Size = New System.Drawing.Size(58, 17)
        Me.CheckFloor3.TabIndex = 2603
        Me.CheckFloor3.Text = "Floor 3"
        Me.CheckFloor3.UseVisualStyleBackColor = True
        '
        'CheckFloor2
        '
        Me.CheckFloor2.AutoSize = True
        Me.CheckFloor2.Location = New System.Drawing.Point(364, 177)
        Me.CheckFloor2.Name = "CheckFloor2"
        Me.CheckFloor2.Size = New System.Drawing.Size(58, 17)
        Me.CheckFloor2.TabIndex = 2602
        Me.CheckFloor2.Text = "Floor 2"
        Me.CheckFloor2.UseVisualStyleBackColor = True
        '
        'CheckFloor1
        '
        Me.CheckFloor1.AutoSize = True
        Me.CheckFloor1.Location = New System.Drawing.Point(364, 154)
        Me.CheckFloor1.Name = "CheckFloor1"
        Me.CheckFloor1.Size = New System.Drawing.Size(58, 17)
        Me.CheckFloor1.TabIndex = 2601
        Me.CheckFloor1.Text = "Floor 1"
        Me.CheckFloor1.UseVisualStyleBackColor = True
        '
        'CheckCamera3
        '
        Me.CheckCamera3.AutoSize = True
        Me.CheckCamera3.Location = New System.Drawing.Point(364, 132)
        Me.CheckCamera3.Name = "CheckCamera3"
        Me.CheckCamera3.Size = New System.Drawing.Size(71, 17)
        Me.CheckCamera3.TabIndex = 2600
        Me.CheckCamera3.Text = "Camera 3"
        Me.CheckCamera3.UseVisualStyleBackColor = True
        '
        'CheckCamera2
        '
        Me.CheckCamera2.AutoSize = True
        Me.CheckCamera2.Location = New System.Drawing.Point(364, 110)
        Me.CheckCamera2.Name = "CheckCamera2"
        Me.CheckCamera2.Size = New System.Drawing.Size(71, 17)
        Me.CheckCamera2.TabIndex = 2599
        Me.CheckCamera2.Text = "Camera 2"
        Me.CheckCamera2.UseVisualStyleBackColor = True
        '
        'CheckCamera1
        '
        Me.CheckCamera1.AutoSize = True
        Me.CheckCamera1.Location = New System.Drawing.Point(364, 88)
        Me.CheckCamera1.Name = "CheckCamera1"
        Me.CheckCamera1.Size = New System.Drawing.Size(71, 17)
        Me.CheckCamera1.TabIndex = 2598
        Me.CheckCamera1.Text = "Camera 1"
        Me.CheckCamera1.UseVisualStyleBackColor = True
        '
        'CheckAudio2
        '
        Me.CheckAudio2.AutoSize = True
        Me.CheckAudio2.Location = New System.Drawing.Point(364, 66)
        Me.CheckAudio2.Name = "CheckAudio2"
        Me.CheckAudio2.Size = New System.Drawing.Size(62, 17)
        Me.CheckAudio2.TabIndex = 2597
        Me.CheckAudio2.Text = "Audio 2"
        Me.CheckAudio2.UseVisualStyleBackColor = True
        '
        'CheckAudio1
        '
        Me.CheckAudio1.AutoSize = True
        Me.CheckAudio1.Location = New System.Drawing.Point(364, 45)
        Me.CheckAudio1.Name = "CheckAudio1"
        Me.CheckAudio1.Size = New System.Drawing.Size(62, 17)
        Me.CheckAudio1.TabIndex = 2596
        Me.CheckAudio1.Text = "Audio 1"
        Me.CheckAudio1.UseVisualStyleBackColor = True
        '
        'CheckGraphics2
        '
        Me.CheckGraphics2.AutoSize = True
        Me.CheckGraphics2.Location = New System.Drawing.Point(271, 199)
        Me.CheckGraphics2.Name = "CheckGraphics2"
        Me.CheckGraphics2.Size = New System.Drawing.Size(77, 17)
        Me.CheckGraphics2.TabIndex = 2595
        Me.CheckGraphics2.Text = "Graphics 2"
        Me.CheckGraphics2.UseVisualStyleBackColor = True
        '
        'CheckGraphics1
        '
        Me.CheckGraphics1.AutoSize = True
        Me.CheckGraphics1.Location = New System.Drawing.Point(271, 177)
        Me.CheckGraphics1.Name = "CheckGraphics1"
        Me.CheckGraphics1.Size = New System.Drawing.Size(77, 17)
        Me.CheckGraphics1.TabIndex = 2594
        Me.CheckGraphics1.Text = "Graphics 1"
        Me.CheckGraphics1.UseVisualStyleBackColor = True
        '
        'CheckDirector2
        '
        Me.CheckDirector2.AutoSize = True
        Me.CheckDirector2.Location = New System.Drawing.Point(271, 154)
        Me.CheckDirector2.Name = "CheckDirector2"
        Me.CheckDirector2.Size = New System.Drawing.Size(72, 17)
        Me.CheckDirector2.TabIndex = 2593
        Me.CheckDirector2.Text = "Director 2"
        Me.CheckDirector2.UseVisualStyleBackColor = True
        '
        'CheckDirector1
        '
        Me.CheckDirector1.AutoSize = True
        Me.CheckDirector1.Location = New System.Drawing.Point(271, 133)
        Me.CheckDirector1.Name = "CheckDirector1"
        Me.CheckDirector1.Size = New System.Drawing.Size(72, 17)
        Me.CheckDirector1.TabIndex = 2592
        Me.CheckDirector1.Text = "Director 1"
        Me.CheckDirector1.UseVisualStyleBackColor = True
        '
        'CheckHost2
        '
        Me.CheckHost2.AutoSize = True
        Me.CheckHost2.Location = New System.Drawing.Point(271, 110)
        Me.CheckHost2.Name = "CheckHost2"
        Me.CheckHost2.Size = New System.Drawing.Size(57, 17)
        Me.CheckHost2.TabIndex = 2591
        Me.CheckHost2.Text = "Host 2"
        Me.CheckHost2.UseVisualStyleBackColor = True
        '
        'CheckHost1
        '
        Me.CheckHost1.AutoSize = True
        Me.CheckHost1.Location = New System.Drawing.Point(271, 88)
        Me.CheckHost1.Name = "CheckHost1"
        Me.CheckHost1.Size = New System.Drawing.Size(57, 17)
        Me.CheckHost1.TabIndex = 2590
        Me.CheckHost1.Text = "Host 1"
        Me.CheckHost1.UseVisualStyleBackColor = True
        '
        'CheckProducer2
        '
        Me.CheckProducer2.AutoSize = True
        Me.CheckProducer2.Location = New System.Drawing.Point(271, 66)
        Me.CheckProducer2.Name = "CheckProducer2"
        Me.CheckProducer2.Size = New System.Drawing.Size(78, 17)
        Me.CheckProducer2.TabIndex = 2589
        Me.CheckProducer2.Text = "Producer 2"
        Me.CheckProducer2.UseVisualStyleBackColor = True
        '
        'CheckProducer1
        '
        Me.CheckProducer1.AutoSize = True
        Me.CheckProducer1.Location = New System.Drawing.Point(271, 45)
        Me.CheckProducer1.Name = "CheckProducer1"
        Me.CheckProducer1.Size = New System.Drawing.Size(78, 17)
        Me.CheckProducer1.TabIndex = 2588
        Me.CheckProducer1.Text = "Producer 1"
        Me.CheckProducer1.UseVisualStyleBackColor = True
        '
        'CheckShowTitle
        '
        Me.CheckShowTitle.AutoSize = True
        Me.CheckShowTitle.Location = New System.Drawing.Point(506, 222)
        Me.CheckShowTitle.Name = "CheckShowTitle"
        Me.CheckShowTitle.Size = New System.Drawing.Size(76, 17)
        Me.CheckShowTitle.TabIndex = 2612
        Me.CheckShowTitle.Text = "Show Title"
        Me.CheckShowTitle.UseVisualStyleBackColor = True
        '
        'CheckRecordDate
        '
        Me.CheckRecordDate.AutoSize = True
        Me.CheckRecordDate.Location = New System.Drawing.Point(506, 199)
        Me.CheckRecordDate.Name = "CheckRecordDate"
        Me.CheckRecordDate.Size = New System.Drawing.Size(87, 17)
        Me.CheckRecordDate.TabIndex = 2611
        Me.CheckRecordDate.Text = "Record Date"
        Me.CheckRecordDate.UseVisualStyleBackColor = True
        '
        'CheckShowYouTube
        '
        Me.CheckShowYouTube.AutoSize = True
        Me.CheckShowYouTube.Location = New System.Drawing.Point(506, 177)
        Me.CheckShowYouTube.Name = "CheckShowYouTube"
        Me.CheckShowYouTube.Size = New System.Drawing.Size(100, 17)
        Me.CheckShowYouTube.TabIndex = 2610
        Me.CheckShowYouTube.Text = "Show YouTube"
        Me.CheckShowYouTube.UseVisualStyleBackColor = True
        '
        'CheckShowTwitter
        '
        Me.CheckShowTwitter.AutoSize = True
        Me.CheckShowTwitter.Location = New System.Drawing.Point(506, 154)
        Me.CheckShowTwitter.Name = "CheckShowTwitter"
        Me.CheckShowTwitter.Size = New System.Drawing.Size(88, 17)
        Me.CheckShowTwitter.TabIndex = 2609
        Me.CheckShowTwitter.Text = "Show Twitter"
        Me.CheckShowTwitter.UseVisualStyleBackColor = True
        '
        'CheckShowFacebook
        '
        Me.CheckShowFacebook.AutoSize = True
        Me.CheckShowFacebook.Location = New System.Drawing.Point(506, 132)
        Me.CheckShowFacebook.Name = "CheckShowFacebook"
        Me.CheckShowFacebook.Size = New System.Drawing.Size(104, 17)
        Me.CheckShowFacebook.TabIndex = 2608
        Me.CheckShowFacebook.Text = "Show Facebook"
        Me.CheckShowFacebook.UseVisualStyleBackColor = True
        '
        'CheckShowWebsite
        '
        Me.CheckShowWebsite.AutoSize = True
        Me.CheckShowWebsite.Location = New System.Drawing.Point(506, 110)
        Me.CheckShowWebsite.Name = "CheckShowWebsite"
        Me.CheckShowWebsite.Size = New System.Drawing.Size(95, 17)
        Me.CheckShowWebsite.TabIndex = 2607
        Me.CheckShowWebsite.Text = "Show Website"
        Me.CheckShowWebsite.UseVisualStyleBackColor = True
        '
        'CheckShowemail
        '
        Me.CheckShowemail.AutoSize = True
        Me.CheckShowemail.Location = New System.Drawing.Point(506, 89)
        Me.CheckShowemail.Name = "CheckShowemail"
        Me.CheckShowemail.Size = New System.Drawing.Size(80, 17)
        Me.CheckShowemail.TabIndex = 2606
        Me.CheckShowemail.Text = "Show email"
        Me.CheckShowemail.UseVisualStyleBackColor = True
        '
        'CheckShowPhone
        '
        Me.CheckShowPhone.AutoSize = True
        Me.CheckShowPhone.Location = New System.Drawing.Point(506, 66)
        Me.CheckShowPhone.Name = "CheckShowPhone"
        Me.CheckShowPhone.Size = New System.Drawing.Size(87, 17)
        Me.CheckShowPhone.TabIndex = 2605
        Me.CheckShowPhone.Text = "Show Phone"
        Me.CheckShowPhone.UseVisualStyleBackColor = True
        '
        'CheckCommentLine
        '
        Me.CheckCommentLine.AutoSize = True
        Me.CheckCommentLine.Location = New System.Drawing.Point(506, 45)
        Me.CheckCommentLine.Name = "CheckCommentLine"
        Me.CheckCommentLine.Size = New System.Drawing.Size(93, 17)
        Me.CheckCommentLine.TabIndex = 2604
        Me.CheckCommentLine.Text = "Comment Line"
        Me.CheckCommentLine.UseVisualStyleBackColor = True
        '
        'LBGuestFields
        '
        Me.LBGuestFields.AutoSize = True
        Me.LBGuestFields.Location = New System.Drawing.Point(18, 18)
        Me.LBGuestFields.Name = "LBGuestFields"
        Me.LBGuestFields.Size = New System.Drawing.Size(65, 13)
        Me.LBGuestFields.TabIndex = 2613
        Me.LBGuestFields.Text = "Guest Fields"
        Me.LBGuestFields.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBCrewFields
        '
        Me.LBCrewFields.AutoSize = True
        Me.LBCrewFields.Location = New System.Drawing.Point(268, 18)
        Me.LBCrewFields.Name = "LBCrewFields"
        Me.LBCrewFields.Size = New System.Drawing.Size(61, 13)
        Me.LBCrewFields.TabIndex = 2614
        Me.LBCrewFields.Text = "Crew Fields"
        Me.LBCrewFields.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBShowFields
        '
        Me.LBShowFields.AutoSize = True
        Me.LBShowFields.Location = New System.Drawing.Point(503, 18)
        Me.LBShowFields.Name = "LBShowFields"
        Me.LBShowFields.Size = New System.Drawing.Size(64, 13)
        Me.LBShowFields.TabIndex = 2615
        Me.LBShowFields.Text = "Show Fields"
        Me.LBShowFields.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CheckOfficeemail
        '
        Me.CheckOfficeemail.AutoSize = True
        Me.CheckOfficeemail.Location = New System.Drawing.Point(101, 285)
        Me.CheckOfficeemail.Name = "CheckOfficeemail"
        Me.CheckOfficeemail.Size = New System.Drawing.Size(81, 17)
        Me.CheckOfficeemail.TabIndex = 2630
        Me.CheckOfficeemail.Text = "Office email"
        Me.CheckOfficeemail.UseVisualStyleBackColor = True
        '
        'CheckOfficePhone
        '
        Me.CheckOfficePhone.AutoSize = True
        Me.CheckOfficePhone.Location = New System.Drawing.Point(101, 264)
        Me.CheckOfficePhone.Name = "CheckOfficePhone"
        Me.CheckOfficePhone.Size = New System.Drawing.Size(88, 17)
        Me.CheckOfficePhone.TabIndex = 2629
        Me.CheckOfficePhone.Text = "Office Phone"
        Me.CheckOfficePhone.UseVisualStyleBackColor = True
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(636, 319)
        Me.Controls.Add(Me.CheckOfficeemail)
        Me.Controls.Add(Me.CheckOfficePhone)
        Me.Controls.Add(Me.LBShowFields)
        Me.Controls.Add(Me.LBCrewFields)
        Me.Controls.Add(Me.LBGuestFields)
        Me.Controls.Add(Me.CheckShowTitle)
        Me.Controls.Add(Me.CheckRecordDate)
        Me.Controls.Add(Me.CheckShowYouTube)
        Me.Controls.Add(Me.CheckShowTwitter)
        Me.Controls.Add(Me.CheckShowFacebook)
        Me.Controls.Add(Me.CheckShowWebsite)
        Me.Controls.Add(Me.CheckShowemail)
        Me.Controls.Add(Me.CheckShowPhone)
        Me.Controls.Add(Me.CheckCommentLine)
        Me.Controls.Add(Me.CheckFloor3)
        Me.Controls.Add(Me.CheckFloor2)
        Me.Controls.Add(Me.CheckFloor1)
        Me.Controls.Add(Me.CheckCamera3)
        Me.Controls.Add(Me.CheckCamera2)
        Me.Controls.Add(Me.CheckCamera1)
        Me.Controls.Add(Me.CheckAudio2)
        Me.Controls.Add(Me.CheckAudio1)
        Me.Controls.Add(Me.CheckGraphics2)
        Me.Controls.Add(Me.CheckGraphics1)
        Me.Controls.Add(Me.CheckDirector2)
        Me.Controls.Add(Me.CheckDirector1)
        Me.Controls.Add(Me.CheckHost2)
        Me.Controls.Add(Me.CheckHost1)
        Me.Controls.Add(Me.CheckProducer2)
        Me.Controls.Add(Me.CheckProducer1)
        Me.Controls.Add(Me.CheckCityWebsite)
        Me.Controls.Add(Me.CheckCityPhone)
        Me.Controls.Add(Me.CheckYouTube)
        Me.Controls.Add(Me.CheckTwitter)
        Me.Controls.Add(Me.CheckFacebook)
        Me.Controls.Add(Me.CheckWebsite)
        Me.Controls.Add(Me.Checkemail)
        Me.Controls.Add(Me.CheckPhone)
        Me.Controls.Add(Me.CheckTitle)
        Me.Controls.Add(Me.CheckName)
        Me.Controls.Add(Me.CheckGuest6)
        Me.Controls.Add(Me.CheckGuest5)
        Me.Controls.Add(Me.CheckGuest4)
        Me.Controls.Add(Me.CheckGuest3)
        Me.Controls.Add(Me.CheckGuest2)
        Me.Controls.Add(Me.CheckGuest1)
        Me.Controls.Add(Me.ButtonSave)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Settings"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Settings"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonSave As Button
    Friend WithEvents CheckCityWebsite As CheckBox
    Friend WithEvents CheckCityPhone As CheckBox
    Friend WithEvents CheckYouTube As CheckBox
    Friend WithEvents CheckTwitter As CheckBox
    Friend WithEvents CheckFacebook As CheckBox
    Friend WithEvents CheckWebsite As CheckBox
    Friend WithEvents Checkemail As CheckBox
    Friend WithEvents CheckPhone As CheckBox
    Friend WithEvents CheckTitle As CheckBox
    Friend WithEvents CheckName As CheckBox
    Friend WithEvents CheckGuest6 As CheckBox
    Friend WithEvents CheckGuest5 As CheckBox
    Friend WithEvents CheckGuest4 As CheckBox
    Friend WithEvents CheckGuest3 As CheckBox
    Friend WithEvents CheckGuest2 As CheckBox
    Friend WithEvents CheckGuest1 As CheckBox
    Friend WithEvents CheckFloor3 As CheckBox
    Friend WithEvents CheckFloor2 As CheckBox
    Friend WithEvents CheckFloor1 As CheckBox
    Friend WithEvents CheckCamera3 As CheckBox
    Friend WithEvents CheckCamera2 As CheckBox
    Friend WithEvents CheckCamera1 As CheckBox
    Friend WithEvents CheckAudio2 As CheckBox
    Friend WithEvents CheckAudio1 As CheckBox
    Friend WithEvents CheckGraphics2 As CheckBox
    Friend WithEvents CheckGraphics1 As CheckBox
    Friend WithEvents CheckDirector2 As CheckBox
    Friend WithEvents CheckDirector1 As CheckBox
    Friend WithEvents CheckHost2 As CheckBox
    Friend WithEvents CheckHost1 As CheckBox
    Friend WithEvents CheckProducer2 As CheckBox
    Friend WithEvents CheckProducer1 As CheckBox
    Friend WithEvents CheckShowTitle As CheckBox
    Friend WithEvents CheckRecordDate As CheckBox
    Friend WithEvents CheckShowYouTube As CheckBox
    Friend WithEvents CheckShowTwitter As CheckBox
    Friend WithEvents CheckShowFacebook As CheckBox
    Friend WithEvents CheckShowWebsite As CheckBox
    Friend WithEvents CheckShowemail As CheckBox
    Friend WithEvents CheckShowPhone As CheckBox
    Friend WithEvents CheckCommentLine As CheckBox
    Friend WithEvents LBGuestFields As Label
    Friend WithEvents LBCrewFields As Label
    Friend WithEvents LBShowFields As Label
    Friend WithEvents CheckOfficeemail As CheckBox
    Friend WithEvents CheckOfficePhone As CheckBox
End Class
