﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.LBGuest1 = New System.Windows.Forms.Label()
        Me.Name1 = New System.Windows.Forms.TextBox()
        Me.LBName1 = New System.Windows.Forms.Label()
        Me.LBTitle1 = New System.Windows.Forms.Label()
        Me.Title1 = New System.Windows.Forms.TextBox()
        Me.LBPhone1 = New System.Windows.Forms.Label()
        Me.Phone1 = New System.Windows.Forms.TextBox()
        Me.LBemail1 = New System.Windows.Forms.Label()
        Me.email1 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone1 = New System.Windows.Forms.Label()
        Me.CityPhone1 = New System.Windows.Forms.TextBox()
        Me.LBWebsite1 = New System.Windows.Forms.Label()
        Me.Website1 = New System.Windows.Forms.TextBox()
        Me.LBCityWebsite1 = New System.Windows.Forms.Label()
        Me.CityWebsite1 = New System.Windows.Forms.TextBox()
        Me.LBCityWebsite2 = New System.Windows.Forms.Label()
        Me.CityWebsite2 = New System.Windows.Forms.TextBox()
        Me.LBWebsite2 = New System.Windows.Forms.Label()
        Me.Website2 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone2 = New System.Windows.Forms.Label()
        Me.CityPhone2 = New System.Windows.Forms.TextBox()
        Me.LBemail2 = New System.Windows.Forms.Label()
        Me.email2 = New System.Windows.Forms.TextBox()
        Me.LBPhone2 = New System.Windows.Forms.Label()
        Me.Phone2 = New System.Windows.Forms.TextBox()
        Me.LBTitle2 = New System.Windows.Forms.Label()
        Me.Title2 = New System.Windows.Forms.TextBox()
        Me.LBName2 = New System.Windows.Forms.Label()
        Me.Name2 = New System.Windows.Forms.TextBox()
        Me.LBGuest2 = New System.Windows.Forms.Label()
        Me.LBCityWebsite3 = New System.Windows.Forms.Label()
        Me.CityWebsite3 = New System.Windows.Forms.TextBox()
        Me.LBWebsite3 = New System.Windows.Forms.Label()
        Me.Website3 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone3 = New System.Windows.Forms.Label()
        Me.CityPhone3 = New System.Windows.Forms.TextBox()
        Me.LBemail3 = New System.Windows.Forms.Label()
        Me.email3 = New System.Windows.Forms.TextBox()
        Me.LBPhone3 = New System.Windows.Forms.Label()
        Me.Phone3 = New System.Windows.Forms.TextBox()
        Me.LBTitle3 = New System.Windows.Forms.Label()
        Me.Title3 = New System.Windows.Forms.TextBox()
        Me.LBName3 = New System.Windows.Forms.Label()
        Me.Name3 = New System.Windows.Forms.TextBox()
        Me.LBGuest3 = New System.Windows.Forms.Label()
        Me.LBCityWebsite6 = New System.Windows.Forms.Label()
        Me.CityWebsite6 = New System.Windows.Forms.TextBox()
        Me.LBWebsite6 = New System.Windows.Forms.Label()
        Me.Website6 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone6 = New System.Windows.Forms.Label()
        Me.CityPhone6 = New System.Windows.Forms.TextBox()
        Me.LBemail6 = New System.Windows.Forms.Label()
        Me.email6 = New System.Windows.Forms.TextBox()
        Me.LBPhone6 = New System.Windows.Forms.Label()
        Me.Phone6 = New System.Windows.Forms.TextBox()
        Me.LBTitle6 = New System.Windows.Forms.Label()
        Me.Title6 = New System.Windows.Forms.TextBox()
        Me.LBName6 = New System.Windows.Forms.Label()
        Me.Name6 = New System.Windows.Forms.TextBox()
        Me.LBGuest6 = New System.Windows.Forms.Label()
        Me.LBCityWebsite5 = New System.Windows.Forms.Label()
        Me.CityWebsite5 = New System.Windows.Forms.TextBox()
        Me.LBWebsite5 = New System.Windows.Forms.Label()
        Me.Website5 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone5 = New System.Windows.Forms.Label()
        Me.CityPhone5 = New System.Windows.Forms.TextBox()
        Me.LBemail5 = New System.Windows.Forms.Label()
        Me.email5 = New System.Windows.Forms.TextBox()
        Me.LBPhone5 = New System.Windows.Forms.Label()
        Me.Phone5 = New System.Windows.Forms.TextBox()
        Me.LBTitle5 = New System.Windows.Forms.Label()
        Me.Title5 = New System.Windows.Forms.TextBox()
        Me.LBName5 = New System.Windows.Forms.Label()
        Me.Name5 = New System.Windows.Forms.TextBox()
        Me.LBGuest5 = New System.Windows.Forms.Label()
        Me.LBCityWebsite4 = New System.Windows.Forms.Label()
        Me.CityWebsite4 = New System.Windows.Forms.TextBox()
        Me.LBWebsite4 = New System.Windows.Forms.Label()
        Me.Website4 = New System.Windows.Forms.TextBox()
        Me.LBCityPhone4 = New System.Windows.Forms.Label()
        Me.CityPhone4 = New System.Windows.Forms.TextBox()
        Me.LBemail4 = New System.Windows.Forms.Label()
        Me.email4 = New System.Windows.Forms.TextBox()
        Me.LBPhone4 = New System.Windows.Forms.Label()
        Me.Phone4 = New System.Windows.Forms.TextBox()
        Me.LBTitle4 = New System.Windows.Forms.Label()
        Me.Title4 = New System.Windows.Forms.TextBox()
        Me.LBName4 = New System.Windows.Forms.Label()
        Me.Name4 = New System.Windows.Forms.TextBox()
        Me.LBGuest4 = New System.Windows.Forms.Label()
        Me.LBRecordDate = New System.Windows.Forms.Label()
        Me.RecordDate = New System.Windows.Forms.TextBox()
        Me.LBShowYouTube = New System.Windows.Forms.Label()
        Me.ShowYouTube = New System.Windows.Forms.TextBox()
        Me.LBCommentLine = New System.Windows.Forms.Label()
        Me.CommentLine = New System.Windows.Forms.TextBox()
        Me.LBShowPhone = New System.Windows.Forms.Label()
        Me.ShowPhone = New System.Windows.Forms.TextBox()
        Me.LBHost1 = New System.Windows.Forms.Label()
        Me.Host1 = New System.Windows.Forms.TextBox()
        Me.LBProducer1 = New System.Windows.Forms.Label()
        Me.Producer1 = New System.Windows.Forms.TextBox()
        Me.LBDirector2 = New System.Windows.Forms.Label()
        Me.Director2 = New System.Windows.Forms.TextBox()
        Me.LBDirector1 = New System.Windows.Forms.Label()
        Me.Director1 = New System.Windows.Forms.TextBox()
        Me.LBGraphics2 = New System.Windows.Forms.Label()
        Me.Graphics2 = New System.Windows.Forms.TextBox()
        Me.LBGraphics1 = New System.Windows.Forms.Label()
        Me.Graphics1 = New System.Windows.Forms.TextBox()
        Me.LBAudio2 = New System.Windows.Forms.Label()
        Me.Audio2 = New System.Windows.Forms.TextBox()
        Me.LBAudio1 = New System.Windows.Forms.Label()
        Me.Audio1 = New System.Windows.Forms.TextBox()
        Me.LBCamera3 = New System.Windows.Forms.Label()
        Me.Camera3 = New System.Windows.Forms.TextBox()
        Me.LBCamera2 = New System.Windows.Forms.Label()
        Me.Camera2 = New System.Windows.Forms.TextBox()
        Me.LBCamera1 = New System.Windows.Forms.Label()
        Me.Camera1 = New System.Windows.Forms.TextBox()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.DB1 = New System.Windows.Forms.RadioButton()
        Me.DB2 = New System.Windows.Forms.RadioButton()
        Me.DB3 = New System.Windows.Forms.RadioButton()
        Me.DB4 = New System.Windows.Forms.RadioButton()
        Me.DB5 = New System.Windows.Forms.RadioButton()
        Me.DB6 = New System.Windows.Forms.RadioButton()
        Me.DBbox = New System.Windows.Forms.ComboBox()
        Me.DBload = New System.Windows.Forms.Button()
        Me.Clear = New System.Windows.Forms.Button()
        Me.SaveDB = New System.Windows.Forms.Button()
        Me.LBShowTitle = New System.Windows.Forms.Label()
        Me.ShowTitle = New System.Windows.Forms.TextBox()
        Me.LBGuestDB = New System.Windows.Forms.Label()
        Me.CDBSave = New System.Windows.Forms.Button()
        Me.CDBload = New System.Windows.Forms.Button()
        Me.LBCDB = New System.Windows.Forms.Label()
        Me.CDBbox = New System.Windows.Forms.ComboBox()
        Me.CDBProducer1 = New System.Windows.Forms.RadioButton()
        Me.CDBHost1 = New System.Windows.Forms.RadioButton()
        Me.CDBDirector2 = New System.Windows.Forms.RadioButton()
        Me.CDBDirector1 = New System.Windows.Forms.RadioButton()
        Me.CDBGraphics2 = New System.Windows.Forms.RadioButton()
        Me.CDBGraphics1 = New System.Windows.Forms.RadioButton()
        Me.CDBCamera3 = New System.Windows.Forms.RadioButton()
        Me.CDBCamera2 = New System.Windows.Forms.RadioButton()
        Me.CDBCamera1 = New System.Windows.Forms.RadioButton()
        Me.CDBAudio2 = New System.Windows.Forms.RadioButton()
        Me.CDBAudio1 = New System.Windows.Forms.RadioButton()
        Me.TextFileCheckTime = New System.Windows.Forms.Timer(Me.components)
        Me.LBShowTwitter = New System.Windows.Forms.Label()
        Me.ShowTwitter = New System.Windows.Forms.TextBox()
        Me.LBShowFacebook = New System.Windows.Forms.Label()
        Me.ShowFacebook = New System.Windows.Forms.TextBox()
        Me.LBShowWebsite = New System.Windows.Forms.Label()
        Me.ShowWebsite = New System.Windows.Forms.TextBox()
        Me.LBShowName = New System.Windows.Forms.Label()
        Me.ShowName = New System.Windows.Forms.TextBox()
        Me.LBShowemail = New System.Windows.Forms.Label()
        Me.Showemail = New System.Windows.Forms.TextBox()
        Me.CDBFloor3 = New System.Windows.Forms.RadioButton()
        Me.CDBFloor2 = New System.Windows.Forms.RadioButton()
        Me.CDBFloor1 = New System.Windows.Forms.RadioButton()
        Me.LBFloor3 = New System.Windows.Forms.Label()
        Me.Floor3 = New System.Windows.Forms.TextBox()
        Me.LBFloor2 = New System.Windows.Forms.Label()
        Me.Floor2 = New System.Windows.Forms.TextBox()
        Me.LBFloor1 = New System.Windows.Forms.Label()
        Me.Floor1 = New System.Windows.Forms.TextBox()
        Me.CDBProducer2 = New System.Windows.Forms.RadioButton()
        Me.LBProducer2 = New System.Windows.Forms.Label()
        Me.Producer2 = New System.Windows.Forms.TextBox()
        Me.CDBHost2 = New System.Windows.Forms.RadioButton()
        Me.LBHost2 = New System.Windows.Forms.Label()
        Me.Host2 = New System.Windows.Forms.TextBox()
        Me.LBTwitter3 = New System.Windows.Forms.Label()
        Me.Twitter3 = New System.Windows.Forms.TextBox()
        Me.LBFacebook3 = New System.Windows.Forms.Label()
        Me.Facebook3 = New System.Windows.Forms.TextBox()
        Me.LBTwitter2 = New System.Windows.Forms.Label()
        Me.Twitter2 = New System.Windows.Forms.TextBox()
        Me.LBFacebook2 = New System.Windows.Forms.Label()
        Me.Facebook2 = New System.Windows.Forms.TextBox()
        Me.LBTwitter1 = New System.Windows.Forms.Label()
        Me.Twitter1 = New System.Windows.Forms.TextBox()
        Me.LBFacebook1 = New System.Windows.Forms.Label()
        Me.Facebook1 = New System.Windows.Forms.TextBox()
        Me.LBTwitter6 = New System.Windows.Forms.Label()
        Me.Twitter6 = New System.Windows.Forms.TextBox()
        Me.LBFacebook6 = New System.Windows.Forms.Label()
        Me.Facebook6 = New System.Windows.Forms.TextBox()
        Me.LBTwitter5 = New System.Windows.Forms.Label()
        Me.Twitter5 = New System.Windows.Forms.TextBox()
        Me.LBFacebook5 = New System.Windows.Forms.Label()
        Me.Facebook5 = New System.Windows.Forms.TextBox()
        Me.LBTwitter4 = New System.Windows.Forms.Label()
        Me.Twitter4 = New System.Windows.Forms.TextBox()
        Me.LBFacebook4 = New System.Windows.Forms.Label()
        Me.Facebook4 = New System.Windows.Forms.TextBox()
        Me.LBYouTube3 = New System.Windows.Forms.Label()
        Me.YouTube3 = New System.Windows.Forms.TextBox()
        Me.LBYouTube2 = New System.Windows.Forms.Label()
        Me.YouTube2 = New System.Windows.Forms.TextBox()
        Me.LBYouTube1 = New System.Windows.Forms.Label()
        Me.YouTube1 = New System.Windows.Forms.TextBox()
        Me.LBYouTube6 = New System.Windows.Forms.Label()
        Me.YouTube6 = New System.Windows.Forms.TextBox()
        Me.LBYouTube5 = New System.Windows.Forms.Label()
        Me.YouTube5 = New System.Windows.Forms.TextBox()
        Me.LBYouTube4 = New System.Windows.Forms.Label()
        Me.YouTube4 = New System.Windows.Forms.TextBox()
        Me.ButtonSettings = New System.Windows.Forms.Button()
        Me.LBOfficeemail6 = New System.Windows.Forms.Label()
        Me.Officeemail6 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone6 = New System.Windows.Forms.Label()
        Me.OfficePhone6 = New System.Windows.Forms.TextBox()
        Me.LBOfficeemail5 = New System.Windows.Forms.Label()
        Me.Officeemail5 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone5 = New System.Windows.Forms.Label()
        Me.OfficePhone5 = New System.Windows.Forms.TextBox()
        Me.LBOfficeemail4 = New System.Windows.Forms.Label()
        Me.Officeemail4 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone4 = New System.Windows.Forms.Label()
        Me.OfficePhone4 = New System.Windows.Forms.TextBox()
        Me.LBOfficeemail3 = New System.Windows.Forms.Label()
        Me.Officeemail3 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone3 = New System.Windows.Forms.Label()
        Me.OfficePhone3 = New System.Windows.Forms.TextBox()
        Me.LBOfficeemail2 = New System.Windows.Forms.Label()
        Me.Officeemail2 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone2 = New System.Windows.Forms.Label()
        Me.OfficePhone2 = New System.Windows.Forms.TextBox()
        Me.LBOfficeemail1 = New System.Windows.Forms.Label()
        Me.Officeemail1 = New System.Windows.Forms.TextBox()
        Me.LBOfficePhone1 = New System.Windows.Forms.Label()
        Me.OfficePhone1 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'LBGuest1
        '
        Me.LBGuest1.AutoSize = True
        Me.LBGuest1.Location = New System.Drawing.Point(12, 12)
        Me.LBGuest1.Name = "LBGuest1"
        Me.LBGuest1.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest1.TabIndex = 0
        Me.LBGuest1.Text = "Guest 1"
        Me.LBGuest1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Name1
        '
        Me.Name1.Location = New System.Drawing.Point(110, 12)
        Me.Name1.Name = "Name1"
        Me.Name1.Size = New System.Drawing.Size(236, 20)
        Me.Name1.TabIndex = 100
        '
        'LBName1
        '
        Me.LBName1.AutoSize = True
        Me.LBName1.Location = New System.Drawing.Point(65, 16)
        Me.LBName1.Name = "LBName1"
        Me.LBName1.Size = New System.Drawing.Size(44, 13)
        Me.LBName1.TabIndex = 2
        Me.LBName1.Text = "Name 1"
        Me.LBName1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBTitle1
        '
        Me.LBTitle1.AutoSize = True
        Me.LBTitle1.Location = New System.Drawing.Point(73, 37)
        Me.LBTitle1.Name = "LBTitle1"
        Me.LBTitle1.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle1.TabIndex = 4
        Me.LBTitle1.Text = "Title 1"
        Me.LBTitle1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Title1
        '
        Me.Title1.Location = New System.Drawing.Point(110, 34)
        Me.Title1.Name = "Title1"
        Me.Title1.Size = New System.Drawing.Size(236, 20)
        Me.Title1.TabIndex = 105
        '
        'LBPhone1
        '
        Me.LBPhone1.AutoSize = True
        Me.LBPhone1.Location = New System.Drawing.Point(62, 59)
        Me.LBPhone1.Name = "LBPhone1"
        Me.LBPhone1.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone1.TabIndex = 6
        Me.LBPhone1.Text = "Phone 1"
        Me.LBPhone1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Phone1
        '
        Me.Phone1.Location = New System.Drawing.Point(110, 56)
        Me.Phone1.Name = "Phone1"
        Me.Phone1.Size = New System.Drawing.Size(236, 20)
        Me.Phone1.TabIndex = 110
        '
        'LBemail1
        '
        Me.LBemail1.AutoSize = True
        Me.LBemail1.Location = New System.Drawing.Point(69, 81)
        Me.LBemail1.Name = "LBemail1"
        Me.LBemail1.Size = New System.Drawing.Size(40, 13)
        Me.LBemail1.TabIndex = 8
        Me.LBemail1.Text = "email 1"
        Me.LBemail1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'email1
        '
        Me.email1.Location = New System.Drawing.Point(110, 78)
        Me.email1.Name = "email1"
        Me.email1.Size = New System.Drawing.Size(236, 20)
        Me.email1.TabIndex = 115
        '
        'LBCityPhone1
        '
        Me.LBCityPhone1.AutoSize = True
        Me.LBCityPhone1.Location = New System.Drawing.Point(12, 191)
        Me.LBCityPhone1.Name = "LBCityPhone1"
        Me.LBCityPhone1.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone1.TabIndex = 10
        Me.LBCityPhone1.Text = "City/State Phone 1"
        Me.LBCityPhone1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CityPhone1
        '
        Me.CityPhone1.Location = New System.Drawing.Point(110, 188)
        Me.CityPhone1.Name = "CityPhone1"
        Me.CityPhone1.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone1.TabIndex = 140
        '
        'LBWebsite1
        '
        Me.LBWebsite1.AutoSize = True
        Me.LBWebsite1.Location = New System.Drawing.Point(54, 103)
        Me.LBWebsite1.Name = "LBWebsite1"
        Me.LBWebsite1.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite1.TabIndex = 12
        Me.LBWebsite1.Text = "Website 1"
        Me.LBWebsite1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Website1
        '
        Me.Website1.Location = New System.Drawing.Point(110, 100)
        Me.Website1.Name = "Website1"
        Me.Website1.Size = New System.Drawing.Size(236, 20)
        Me.Website1.TabIndex = 120
        '
        'LBCityWebsite1
        '
        Me.LBCityWebsite1.AutoSize = True
        Me.LBCityWebsite1.Location = New System.Drawing.Point(4, 213)
        Me.LBCityWebsite1.Name = "LBCityWebsite1"
        Me.LBCityWebsite1.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite1.TabIndex = 16
        Me.LBCityWebsite1.Text = "City/State Website 1"
        '
        'CityWebsite1
        '
        Me.CityWebsite1.Location = New System.Drawing.Point(110, 210)
        Me.CityWebsite1.Name = "CityWebsite1"
        Me.CityWebsite1.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite1.TabIndex = 145
        '
        'LBCityWebsite2
        '
        Me.LBCityWebsite2.AutoSize = True
        Me.LBCityWebsite2.Location = New System.Drawing.Point(351, 213)
        Me.LBCityWebsite2.Name = "LBCityWebsite2"
        Me.LBCityWebsite2.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite2.TabIndex = 33
        Me.LBCityWebsite2.Text = "City/State Website 2"
        '
        'CityWebsite2
        '
        Me.CityWebsite2.Location = New System.Drawing.Point(457, 210)
        Me.CityWebsite2.Name = "CityWebsite2"
        Me.CityWebsite2.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite2.TabIndex = 245
        '
        'LBWebsite2
        '
        Me.LBWebsite2.AutoSize = True
        Me.LBWebsite2.Location = New System.Drawing.Point(401, 103)
        Me.LBWebsite2.Name = "LBWebsite2"
        Me.LBWebsite2.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite2.TabIndex = 29
        Me.LBWebsite2.Text = "Website 2"
        '
        'Website2
        '
        Me.Website2.Location = New System.Drawing.Point(457, 100)
        Me.Website2.Name = "Website2"
        Me.Website2.Size = New System.Drawing.Size(236, 20)
        Me.Website2.TabIndex = 220
        '
        'LBCityPhone2
        '
        Me.LBCityPhone2.AutoSize = True
        Me.LBCityPhone2.Location = New System.Drawing.Point(359, 191)
        Me.LBCityPhone2.Name = "LBCityPhone2"
        Me.LBCityPhone2.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone2.TabIndex = 27
        Me.LBCityPhone2.Text = "City/State Phone 2"
        '
        'CityPhone2
        '
        Me.CityPhone2.Location = New System.Drawing.Point(457, 188)
        Me.CityPhone2.Name = "CityPhone2"
        Me.CityPhone2.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone2.TabIndex = 240
        '
        'LBemail2
        '
        Me.LBemail2.AutoSize = True
        Me.LBemail2.Location = New System.Drawing.Point(416, 81)
        Me.LBemail2.Name = "LBemail2"
        Me.LBemail2.Size = New System.Drawing.Size(40, 13)
        Me.LBemail2.TabIndex = 25
        Me.LBemail2.Text = "email 2"
        '
        'email2
        '
        Me.email2.Location = New System.Drawing.Point(457, 78)
        Me.email2.Name = "email2"
        Me.email2.Size = New System.Drawing.Size(236, 20)
        Me.email2.TabIndex = 215
        '
        'LBPhone2
        '
        Me.LBPhone2.AutoSize = True
        Me.LBPhone2.Location = New System.Drawing.Point(409, 59)
        Me.LBPhone2.Name = "LBPhone2"
        Me.LBPhone2.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone2.TabIndex = 23
        Me.LBPhone2.Text = "Phone 2"
        '
        'Phone2
        '
        Me.Phone2.Location = New System.Drawing.Point(457, 56)
        Me.Phone2.Name = "Phone2"
        Me.Phone2.Size = New System.Drawing.Size(236, 20)
        Me.Phone2.TabIndex = 210
        '
        'LBTitle2
        '
        Me.LBTitle2.AutoSize = True
        Me.LBTitle2.Location = New System.Drawing.Point(420, 37)
        Me.LBTitle2.Name = "LBTitle2"
        Me.LBTitle2.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle2.TabIndex = 21
        Me.LBTitle2.Text = "Title 2"
        '
        'Title2
        '
        Me.Title2.Location = New System.Drawing.Point(457, 34)
        Me.Title2.Name = "Title2"
        Me.Title2.Size = New System.Drawing.Size(236, 20)
        Me.Title2.TabIndex = 205
        '
        'LBName2
        '
        Me.LBName2.AutoSize = True
        Me.LBName2.Location = New System.Drawing.Point(412, 15)
        Me.LBName2.Name = "LBName2"
        Me.LBName2.Size = New System.Drawing.Size(44, 13)
        Me.LBName2.TabIndex = 19
        Me.LBName2.Text = "Name 2"
        '
        'Name2
        '
        Me.Name2.Location = New System.Drawing.Point(457, 12)
        Me.Name2.Name = "Name2"
        Me.Name2.Size = New System.Drawing.Size(236, 20)
        Me.Name2.TabIndex = 200
        '
        'LBGuest2
        '
        Me.LBGuest2.AutoSize = True
        Me.LBGuest2.Location = New System.Drawing.Point(359, 12)
        Me.LBGuest2.Name = "LBGuest2"
        Me.LBGuest2.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest2.TabIndex = 17
        Me.LBGuest2.Text = "Guest 2"
        Me.LBGuest2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBCityWebsite3
        '
        Me.LBCityWebsite3.AutoSize = True
        Me.LBCityWebsite3.Location = New System.Drawing.Point(700, 213)
        Me.LBCityWebsite3.Name = "LBCityWebsite3"
        Me.LBCityWebsite3.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite3.TabIndex = 50
        Me.LBCityWebsite3.Text = "City/State Website 3"
        '
        'CityWebsite3
        '
        Me.CityWebsite3.Location = New System.Drawing.Point(806, 210)
        Me.CityWebsite3.Name = "CityWebsite3"
        Me.CityWebsite3.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite3.TabIndex = 345
        '
        'LBWebsite3
        '
        Me.LBWebsite3.AutoSize = True
        Me.LBWebsite3.Location = New System.Drawing.Point(750, 103)
        Me.LBWebsite3.Name = "LBWebsite3"
        Me.LBWebsite3.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite3.TabIndex = 46
        Me.LBWebsite3.Text = "Website 3"
        '
        'Website3
        '
        Me.Website3.Location = New System.Drawing.Point(806, 100)
        Me.Website3.Name = "Website3"
        Me.Website3.Size = New System.Drawing.Size(236, 20)
        Me.Website3.TabIndex = 320
        '
        'LBCityPhone3
        '
        Me.LBCityPhone3.AutoSize = True
        Me.LBCityPhone3.Location = New System.Drawing.Point(708, 191)
        Me.LBCityPhone3.Name = "LBCityPhone3"
        Me.LBCityPhone3.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone3.TabIndex = 44
        Me.LBCityPhone3.Text = "City/State Phone 3"
        '
        'CityPhone3
        '
        Me.CityPhone3.Location = New System.Drawing.Point(806, 188)
        Me.CityPhone3.Name = "CityPhone3"
        Me.CityPhone3.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone3.TabIndex = 340
        '
        'LBemail3
        '
        Me.LBemail3.AutoSize = True
        Me.LBemail3.Location = New System.Drawing.Point(765, 81)
        Me.LBemail3.Name = "LBemail3"
        Me.LBemail3.Size = New System.Drawing.Size(40, 13)
        Me.LBemail3.TabIndex = 42
        Me.LBemail3.Text = "email 3"
        '
        'email3
        '
        Me.email3.Location = New System.Drawing.Point(806, 78)
        Me.email3.Name = "email3"
        Me.email3.Size = New System.Drawing.Size(236, 20)
        Me.email3.TabIndex = 315
        '
        'LBPhone3
        '
        Me.LBPhone3.AutoSize = True
        Me.LBPhone3.Location = New System.Drawing.Point(758, 59)
        Me.LBPhone3.Name = "LBPhone3"
        Me.LBPhone3.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone3.TabIndex = 40
        Me.LBPhone3.Text = "Phone 3"
        '
        'Phone3
        '
        Me.Phone3.Location = New System.Drawing.Point(806, 56)
        Me.Phone3.Name = "Phone3"
        Me.Phone3.Size = New System.Drawing.Size(236, 20)
        Me.Phone3.TabIndex = 310
        '
        'LBTitle3
        '
        Me.LBTitle3.AutoSize = True
        Me.LBTitle3.Location = New System.Drawing.Point(769, 37)
        Me.LBTitle3.Name = "LBTitle3"
        Me.LBTitle3.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle3.TabIndex = 38
        Me.LBTitle3.Text = "Title 3"
        '
        'Title3
        '
        Me.Title3.Location = New System.Drawing.Point(806, 34)
        Me.Title3.Name = "Title3"
        Me.Title3.Size = New System.Drawing.Size(236, 20)
        Me.Title3.TabIndex = 305
        '
        'LBName3
        '
        Me.LBName3.AutoSize = True
        Me.LBName3.Location = New System.Drawing.Point(762, 15)
        Me.LBName3.Name = "LBName3"
        Me.LBName3.Size = New System.Drawing.Size(44, 13)
        Me.LBName3.TabIndex = 36
        Me.LBName3.Text = "Name 3"
        '
        'Name3
        '
        Me.Name3.Location = New System.Drawing.Point(806, 12)
        Me.Name3.Name = "Name3"
        Me.Name3.Size = New System.Drawing.Size(236, 20)
        Me.Name3.TabIndex = 300
        '
        'LBGuest3
        '
        Me.LBGuest3.AutoSize = True
        Me.LBGuest3.Location = New System.Drawing.Point(708, 12)
        Me.LBGuest3.Name = "LBGuest3"
        Me.LBGuest3.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest3.TabIndex = 34
        Me.LBGuest3.Text = "Guest 3"
        Me.LBGuest3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBCityWebsite6
        '
        Me.LBCityWebsite6.AutoSize = True
        Me.LBCityWebsite6.Location = New System.Drawing.Point(700, 504)
        Me.LBCityWebsite6.Name = "LBCityWebsite6"
        Me.LBCityWebsite6.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite6.TabIndex = 101
        Me.LBCityWebsite6.Text = "City/State Website 6"
        '
        'CityWebsite6
        '
        Me.CityWebsite6.Location = New System.Drawing.Point(806, 501)
        Me.CityWebsite6.Name = "CityWebsite6"
        Me.CityWebsite6.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite6.TabIndex = 645
        '
        'LBWebsite6
        '
        Me.LBWebsite6.AutoSize = True
        Me.LBWebsite6.Location = New System.Drawing.Point(750, 394)
        Me.LBWebsite6.Name = "LBWebsite6"
        Me.LBWebsite6.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite6.TabIndex = 97
        Me.LBWebsite6.Text = "Website 6"
        '
        'Website6
        '
        Me.Website6.Location = New System.Drawing.Point(806, 391)
        Me.Website6.Name = "Website6"
        Me.Website6.Size = New System.Drawing.Size(236, 20)
        Me.Website6.TabIndex = 620
        '
        'LBCityPhone6
        '
        Me.LBCityPhone6.AutoSize = True
        Me.LBCityPhone6.Location = New System.Drawing.Point(708, 483)
        Me.LBCityPhone6.Name = "LBCityPhone6"
        Me.LBCityPhone6.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone6.TabIndex = 95
        Me.LBCityPhone6.Text = "City/State Phone 6"
        '
        'CityPhone6
        '
        Me.CityPhone6.Location = New System.Drawing.Point(806, 479)
        Me.CityPhone6.Name = "CityPhone6"
        Me.CityPhone6.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone6.TabIndex = 640
        '
        'LBemail6
        '
        Me.LBemail6.AutoSize = True
        Me.LBemail6.Location = New System.Drawing.Point(765, 373)
        Me.LBemail6.Name = "LBemail6"
        Me.LBemail6.Size = New System.Drawing.Size(40, 13)
        Me.LBemail6.TabIndex = 93
        Me.LBemail6.Text = "email 6"
        '
        'email6
        '
        Me.email6.Location = New System.Drawing.Point(806, 369)
        Me.email6.Name = "email6"
        Me.email6.Size = New System.Drawing.Size(236, 20)
        Me.email6.TabIndex = 615
        '
        'LBPhone6
        '
        Me.LBPhone6.AutoSize = True
        Me.LBPhone6.Location = New System.Drawing.Point(758, 350)
        Me.LBPhone6.Name = "LBPhone6"
        Me.LBPhone6.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone6.TabIndex = 91
        Me.LBPhone6.Text = "Phone 6"
        '
        'Phone6
        '
        Me.Phone6.Location = New System.Drawing.Point(806, 347)
        Me.Phone6.Name = "Phone6"
        Me.Phone6.Size = New System.Drawing.Size(236, 20)
        Me.Phone6.TabIndex = 610
        '
        'LBTitle6
        '
        Me.LBTitle6.AutoSize = True
        Me.LBTitle6.Location = New System.Drawing.Point(769, 328)
        Me.LBTitle6.Name = "LBTitle6"
        Me.LBTitle6.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle6.TabIndex = 89
        Me.LBTitle6.Text = "Title 6"
        '
        'Title6
        '
        Me.Title6.Location = New System.Drawing.Point(806, 325)
        Me.Title6.Name = "Title6"
        Me.Title6.Size = New System.Drawing.Size(236, 20)
        Me.Title6.TabIndex = 605
        '
        'LBName6
        '
        Me.LBName6.AutoSize = True
        Me.LBName6.Location = New System.Drawing.Point(761, 306)
        Me.LBName6.Name = "LBName6"
        Me.LBName6.Size = New System.Drawing.Size(44, 13)
        Me.LBName6.TabIndex = 87
        Me.LBName6.Text = "Name 6"
        '
        'Name6
        '
        Me.Name6.Location = New System.Drawing.Point(806, 303)
        Me.Name6.Name = "Name6"
        Me.Name6.Size = New System.Drawing.Size(236, 20)
        Me.Name6.TabIndex = 600
        '
        'LBGuest6
        '
        Me.LBGuest6.AutoSize = True
        Me.LBGuest6.Location = New System.Drawing.Point(708, 300)
        Me.LBGuest6.Name = "LBGuest6"
        Me.LBGuest6.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest6.TabIndex = 85
        Me.LBGuest6.Text = "Guest 6"
        Me.LBGuest6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBCityWebsite5
        '
        Me.LBCityWebsite5.AutoSize = True
        Me.LBCityWebsite5.Location = New System.Drawing.Point(351, 503)
        Me.LBCityWebsite5.Name = "LBCityWebsite5"
        Me.LBCityWebsite5.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite5.TabIndex = 84
        Me.LBCityWebsite5.Text = "City/State Website 5"
        '
        'CityWebsite5
        '
        Me.CityWebsite5.Location = New System.Drawing.Point(457, 501)
        Me.CityWebsite5.Name = "CityWebsite5"
        Me.CityWebsite5.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite5.TabIndex = 545
        '
        'LBWebsite5
        '
        Me.LBWebsite5.AutoSize = True
        Me.LBWebsite5.Location = New System.Drawing.Point(401, 394)
        Me.LBWebsite5.Name = "LBWebsite5"
        Me.LBWebsite5.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite5.TabIndex = 80
        Me.LBWebsite5.Text = "Website 5"
        '
        'Website5
        '
        Me.Website5.Location = New System.Drawing.Point(457, 391)
        Me.Website5.Name = "Website5"
        Me.Website5.Size = New System.Drawing.Size(236, 20)
        Me.Website5.TabIndex = 520
        '
        'LBCityPhone5
        '
        Me.LBCityPhone5.AutoSize = True
        Me.LBCityPhone5.Location = New System.Drawing.Point(359, 482)
        Me.LBCityPhone5.Name = "LBCityPhone5"
        Me.LBCityPhone5.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone5.TabIndex = 78
        Me.LBCityPhone5.Text = "City/State Phone 5"
        '
        'CityPhone5
        '
        Me.CityPhone5.Location = New System.Drawing.Point(457, 479)
        Me.CityPhone5.Name = "CityPhone5"
        Me.CityPhone5.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone5.TabIndex = 540
        '
        'LBemail5
        '
        Me.LBemail5.AutoSize = True
        Me.LBemail5.Location = New System.Drawing.Point(416, 372)
        Me.LBemail5.Name = "LBemail5"
        Me.LBemail5.Size = New System.Drawing.Size(40, 13)
        Me.LBemail5.TabIndex = 76
        Me.LBemail5.Text = "email 5"
        '
        'email5
        '
        Me.email5.Location = New System.Drawing.Point(457, 369)
        Me.email5.Name = "email5"
        Me.email5.Size = New System.Drawing.Size(236, 20)
        Me.email5.TabIndex = 515
        '
        'LBPhone5
        '
        Me.LBPhone5.AutoSize = True
        Me.LBPhone5.Location = New System.Drawing.Point(409, 351)
        Me.LBPhone5.Name = "LBPhone5"
        Me.LBPhone5.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone5.TabIndex = 74
        Me.LBPhone5.Text = "Phone 5"
        '
        'Phone5
        '
        Me.Phone5.Location = New System.Drawing.Point(457, 347)
        Me.Phone5.Name = "Phone5"
        Me.Phone5.Size = New System.Drawing.Size(236, 20)
        Me.Phone5.TabIndex = 510
        '
        'LBTitle5
        '
        Me.LBTitle5.AutoSize = True
        Me.LBTitle5.Location = New System.Drawing.Point(420, 329)
        Me.LBTitle5.Name = "LBTitle5"
        Me.LBTitle5.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle5.TabIndex = 72
        Me.LBTitle5.Text = "Title 5"
        '
        'Title5
        '
        Me.Title5.Location = New System.Drawing.Point(457, 325)
        Me.Title5.Name = "Title5"
        Me.Title5.Size = New System.Drawing.Size(236, 20)
        Me.Title5.TabIndex = 505
        '
        'LBName5
        '
        Me.LBName5.AutoSize = True
        Me.LBName5.Location = New System.Drawing.Point(412, 306)
        Me.LBName5.Name = "LBName5"
        Me.LBName5.Size = New System.Drawing.Size(44, 13)
        Me.LBName5.TabIndex = 70
        Me.LBName5.Text = "Name 5"
        '
        'Name5
        '
        Me.Name5.Location = New System.Drawing.Point(457, 303)
        Me.Name5.Name = "Name5"
        Me.Name5.Size = New System.Drawing.Size(236, 20)
        Me.Name5.TabIndex = 500
        '
        'LBGuest5
        '
        Me.LBGuest5.AutoSize = True
        Me.LBGuest5.Location = New System.Drawing.Point(359, 301)
        Me.LBGuest5.Name = "LBGuest5"
        Me.LBGuest5.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest5.TabIndex = 68
        Me.LBGuest5.Text = "Guest 5"
        Me.LBGuest5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBCityWebsite4
        '
        Me.LBCityWebsite4.AutoSize = True
        Me.LBCityWebsite4.Location = New System.Drawing.Point(4, 504)
        Me.LBCityWebsite4.Name = "LBCityWebsite4"
        Me.LBCityWebsite4.Size = New System.Drawing.Size(105, 13)
        Me.LBCityWebsite4.TabIndex = 67
        Me.LBCityWebsite4.Text = "City/State Website 4"
        '
        'CityWebsite4
        '
        Me.CityWebsite4.Location = New System.Drawing.Point(110, 501)
        Me.CityWebsite4.Name = "CityWebsite4"
        Me.CityWebsite4.Size = New System.Drawing.Size(236, 20)
        Me.CityWebsite4.TabIndex = 445
        '
        'LBWebsite4
        '
        Me.LBWebsite4.AutoSize = True
        Me.LBWebsite4.Location = New System.Drawing.Point(54, 394)
        Me.LBWebsite4.Name = "LBWebsite4"
        Me.LBWebsite4.Size = New System.Drawing.Size(55, 13)
        Me.LBWebsite4.TabIndex = 63
        Me.LBWebsite4.Text = "Website 4"
        '
        'Website4
        '
        Me.Website4.Location = New System.Drawing.Point(110, 391)
        Me.Website4.Name = "Website4"
        Me.Website4.Size = New System.Drawing.Size(236, 20)
        Me.Website4.TabIndex = 420
        '
        'LBCityPhone4
        '
        Me.LBCityPhone4.AutoSize = True
        Me.LBCityPhone4.Location = New System.Drawing.Point(12, 482)
        Me.LBCityPhone4.Name = "LBCityPhone4"
        Me.LBCityPhone4.Size = New System.Drawing.Size(97, 13)
        Me.LBCityPhone4.TabIndex = 61
        Me.LBCityPhone4.Text = "City/State Phone 4"
        '
        'CityPhone4
        '
        Me.CityPhone4.Location = New System.Drawing.Point(110, 479)
        Me.CityPhone4.Name = "CityPhone4"
        Me.CityPhone4.Size = New System.Drawing.Size(236, 20)
        Me.CityPhone4.TabIndex = 440
        '
        'LBemail4
        '
        Me.LBemail4.AutoSize = True
        Me.LBemail4.Location = New System.Drawing.Point(69, 372)
        Me.LBemail4.Name = "LBemail4"
        Me.LBemail4.Size = New System.Drawing.Size(40, 13)
        Me.LBemail4.TabIndex = 59
        Me.LBemail4.Text = "email 4"
        '
        'email4
        '
        Me.email4.Location = New System.Drawing.Point(110, 369)
        Me.email4.Name = "email4"
        Me.email4.Size = New System.Drawing.Size(236, 20)
        Me.email4.TabIndex = 415
        '
        'LBPhone4
        '
        Me.LBPhone4.AutoSize = True
        Me.LBPhone4.Location = New System.Drawing.Point(62, 350)
        Me.LBPhone4.Name = "LBPhone4"
        Me.LBPhone4.Size = New System.Drawing.Size(47, 13)
        Me.LBPhone4.TabIndex = 57
        Me.LBPhone4.Text = "Phone 4"
        '
        'Phone4
        '
        Me.Phone4.Location = New System.Drawing.Point(110, 347)
        Me.Phone4.Name = "Phone4"
        Me.Phone4.Size = New System.Drawing.Size(236, 20)
        Me.Phone4.TabIndex = 410
        '
        'LBTitle4
        '
        Me.LBTitle4.AutoSize = True
        Me.LBTitle4.Location = New System.Drawing.Point(73, 329)
        Me.LBTitle4.Name = "LBTitle4"
        Me.LBTitle4.Size = New System.Drawing.Size(36, 13)
        Me.LBTitle4.TabIndex = 55
        Me.LBTitle4.Text = "Title 4"
        '
        'Title4
        '
        Me.Title4.Location = New System.Drawing.Point(110, 325)
        Me.Title4.Name = "Title4"
        Me.Title4.Size = New System.Drawing.Size(236, 20)
        Me.Title4.TabIndex = 405
        '
        'LBName4
        '
        Me.LBName4.AutoSize = True
        Me.LBName4.Location = New System.Drawing.Point(65, 306)
        Me.LBName4.Name = "LBName4"
        Me.LBName4.Size = New System.Drawing.Size(44, 13)
        Me.LBName4.TabIndex = 53
        Me.LBName4.Text = "Name 4"
        '
        'Name4
        '
        Me.Name4.Location = New System.Drawing.Point(110, 303)
        Me.Name4.Name = "Name4"
        Me.Name4.Size = New System.Drawing.Size(236, 20)
        Me.Name4.TabIndex = 400
        '
        'LBGuest4
        '
        Me.LBGuest4.AutoSize = True
        Me.LBGuest4.Location = New System.Drawing.Point(12, 303)
        Me.LBGuest4.Name = "LBGuest4"
        Me.LBGuest4.Size = New System.Drawing.Size(44, 13)
        Me.LBGuest4.TabIndex = 51
        Me.LBGuest4.Text = "Guest 4"
        Me.LBGuest4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBRecordDate
        '
        Me.LBRecordDate.AutoSize = True
        Me.LBRecordDate.Location = New System.Drawing.Point(736, 790)
        Me.LBRecordDate.Name = "LBRecordDate"
        Me.LBRecordDate.Size = New System.Drawing.Size(68, 13)
        Me.LBRecordDate.TabIndex = 110
        Me.LBRecordDate.Text = "Record Date"
        Me.LBRecordDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'RecordDate
        '
        Me.RecordDate.Location = New System.Drawing.Point(806, 787)
        Me.RecordDate.Name = "RecordDate"
        Me.RecordDate.Size = New System.Drawing.Size(236, 20)
        Me.RecordDate.TabIndex = 935
        '
        'LBShowYouTube
        '
        Me.LBShowYouTube.AutoSize = True
        Me.LBShowYouTube.Location = New System.Drawing.Point(722, 768)
        Me.LBShowYouTube.Name = "LBShowYouTube"
        Me.LBShowYouTube.Size = New System.Drawing.Size(81, 13)
        Me.LBShowYouTube.TabIndex = 108
        Me.LBShowYouTube.Text = "Show YouTube"
        Me.LBShowYouTube.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowYouTube
        '
        Me.ShowYouTube.Location = New System.Drawing.Point(806, 765)
        Me.ShowYouTube.Name = "ShowYouTube"
        Me.ShowYouTube.Size = New System.Drawing.Size(236, 20)
        Me.ShowYouTube.TabIndex = 930
        '
        'LBCommentLine
        '
        Me.LBCommentLine.AutoSize = True
        Me.LBCommentLine.Location = New System.Drawing.Point(730, 636)
        Me.LBCommentLine.Name = "LBCommentLine"
        Me.LBCommentLine.Size = New System.Drawing.Size(74, 13)
        Me.LBCommentLine.TabIndex = 106
        Me.LBCommentLine.Text = "Comment Line"
        Me.LBCommentLine.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CommentLine
        '
        Me.CommentLine.Location = New System.Drawing.Point(806, 633)
        Me.CommentLine.Name = "CommentLine"
        Me.CommentLine.Size = New System.Drawing.Size(236, 20)
        Me.CommentLine.TabIndex = 900
        '
        'LBShowPhone
        '
        Me.LBShowPhone.AutoSize = True
        Me.LBShowPhone.Location = New System.Drawing.Point(736, 658)
        Me.LBShowPhone.Name = "LBShowPhone"
        Me.LBShowPhone.Size = New System.Drawing.Size(68, 13)
        Me.LBShowPhone.TabIndex = 104
        Me.LBShowPhone.Text = "Show Phone"
        Me.LBShowPhone.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowPhone
        '
        Me.ShowPhone.Location = New System.Drawing.Point(806, 655)
        Me.ShowPhone.Name = "ShowPhone"
        Me.ShowPhone.Size = New System.Drawing.Size(236, 20)
        Me.ShowPhone.TabIndex = 905
        '
        'LBHost1
        '
        Me.LBHost1.AutoSize = True
        Me.LBHost1.Location = New System.Drawing.Point(69, 680)
        Me.LBHost1.Name = "LBHost1"
        Me.LBHost1.Size = New System.Drawing.Size(38, 13)
        Me.LBHost1.TabIndex = 123
        Me.LBHost1.Text = "Host 1"
        Me.LBHost1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Host1
        '
        Me.Host1.Location = New System.Drawing.Point(110, 677)
        Me.Host1.Name = "Host1"
        Me.Host1.Size = New System.Drawing.Size(236, 20)
        Me.Host1.TabIndex = 710
        '
        'LBProducer1
        '
        Me.LBProducer1.AutoSize = True
        Me.LBProducer1.Location = New System.Drawing.Point(48, 636)
        Me.LBProducer1.Name = "LBProducer1"
        Me.LBProducer1.Size = New System.Drawing.Size(59, 13)
        Me.LBProducer1.TabIndex = 121
        Me.LBProducer1.Text = "Producer 1"
        Me.LBProducer1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Producer1
        '
        Me.Producer1.Location = New System.Drawing.Point(110, 633)
        Me.Producer1.Name = "Producer1"
        Me.Producer1.Size = New System.Drawing.Size(236, 20)
        Me.Producer1.TabIndex = 700
        '
        'LBDirector2
        '
        Me.LBDirector2.AutoSize = True
        Me.LBDirector2.Location = New System.Drawing.Point(54, 746)
        Me.LBDirector2.Name = "LBDirector2"
        Me.LBDirector2.Size = New System.Drawing.Size(53, 13)
        Me.LBDirector2.TabIndex = 127
        Me.LBDirector2.Text = "Director 2"
        Me.LBDirector2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Director2
        '
        Me.Director2.Location = New System.Drawing.Point(110, 743)
        Me.Director2.Name = "Director2"
        Me.Director2.Size = New System.Drawing.Size(236, 20)
        Me.Director2.TabIndex = 725
        '
        'LBDirector1
        '
        Me.LBDirector1.AutoSize = True
        Me.LBDirector1.Location = New System.Drawing.Point(54, 724)
        Me.LBDirector1.Name = "LBDirector1"
        Me.LBDirector1.Size = New System.Drawing.Size(53, 13)
        Me.LBDirector1.TabIndex = 125
        Me.LBDirector1.Text = "Director 1"
        Me.LBDirector1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Director1
        '
        Me.Director1.Location = New System.Drawing.Point(110, 721)
        Me.Director1.Name = "Director1"
        Me.Director1.Size = New System.Drawing.Size(236, 20)
        Me.Director1.TabIndex = 720
        '
        'LBGraphics2
        '
        Me.LBGraphics2.AutoSize = True
        Me.LBGraphics2.Location = New System.Drawing.Point(49, 790)
        Me.LBGraphics2.Name = "LBGraphics2"
        Me.LBGraphics2.Size = New System.Drawing.Size(58, 13)
        Me.LBGraphics2.TabIndex = 131
        Me.LBGraphics2.Text = "Graphics 2"
        Me.LBGraphics2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Graphics2
        '
        Me.Graphics2.Location = New System.Drawing.Point(110, 787)
        Me.Graphics2.Name = "Graphics2"
        Me.Graphics2.Size = New System.Drawing.Size(236, 20)
        Me.Graphics2.TabIndex = 735
        '
        'LBGraphics1
        '
        Me.LBGraphics1.AutoSize = True
        Me.LBGraphics1.Location = New System.Drawing.Point(49, 768)
        Me.LBGraphics1.Name = "LBGraphics1"
        Me.LBGraphics1.Size = New System.Drawing.Size(58, 13)
        Me.LBGraphics1.TabIndex = 129
        Me.LBGraphics1.Text = "Graphics 1"
        Me.LBGraphics1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Graphics1
        '
        Me.Graphics1.Location = New System.Drawing.Point(110, 765)
        Me.Graphics1.Name = "Graphics1"
        Me.Graphics1.Size = New System.Drawing.Size(236, 20)
        Me.Graphics1.TabIndex = 730
        '
        'LBAudio2
        '
        Me.LBAudio2.AutoSize = True
        Me.LBAudio2.Location = New System.Drawing.Point(410, 658)
        Me.LBAudio2.Name = "LBAudio2"
        Me.LBAudio2.Size = New System.Drawing.Size(43, 13)
        Me.LBAudio2.TabIndex = 135
        Me.LBAudio2.Text = "Audio 2"
        Me.LBAudio2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Audio2
        '
        Me.Audio2.Location = New System.Drawing.Point(457, 655)
        Me.Audio2.Name = "Audio2"
        Me.Audio2.Size = New System.Drawing.Size(236, 20)
        Me.Audio2.TabIndex = 805
        '
        'LBAudio1
        '
        Me.LBAudio1.AutoSize = True
        Me.LBAudio1.Location = New System.Drawing.Point(410, 636)
        Me.LBAudio1.Name = "LBAudio1"
        Me.LBAudio1.Size = New System.Drawing.Size(43, 13)
        Me.LBAudio1.TabIndex = 133
        Me.LBAudio1.Text = "Audio 1"
        Me.LBAudio1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Audio1
        '
        Me.Audio1.Location = New System.Drawing.Point(457, 633)
        Me.Audio1.Name = "Audio1"
        Me.Audio1.Size = New System.Drawing.Size(236, 20)
        Me.Audio1.TabIndex = 800
        '
        'LBCamera3
        '
        Me.LBCamera3.AutoSize = True
        Me.LBCamera3.Location = New System.Drawing.Point(402, 724)
        Me.LBCamera3.Name = "LBCamera3"
        Me.LBCamera3.Size = New System.Drawing.Size(52, 13)
        Me.LBCamera3.TabIndex = 141
        Me.LBCamera3.Text = "Camera 3"
        Me.LBCamera3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Camera3
        '
        Me.Camera3.Location = New System.Drawing.Point(457, 721)
        Me.Camera3.Name = "Camera3"
        Me.Camera3.Size = New System.Drawing.Size(236, 20)
        Me.Camera3.TabIndex = 820
        '
        'LBCamera2
        '
        Me.LBCamera2.AutoSize = True
        Me.LBCamera2.Location = New System.Drawing.Point(402, 702)
        Me.LBCamera2.Name = "LBCamera2"
        Me.LBCamera2.Size = New System.Drawing.Size(52, 13)
        Me.LBCamera2.TabIndex = 139
        Me.LBCamera2.Text = "Camera 2"
        Me.LBCamera2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Camera2
        '
        Me.Camera2.Location = New System.Drawing.Point(457, 699)
        Me.Camera2.Name = "Camera2"
        Me.Camera2.Size = New System.Drawing.Size(236, 20)
        Me.Camera2.TabIndex = 815
        '
        'LBCamera1
        '
        Me.LBCamera1.AutoSize = True
        Me.LBCamera1.Location = New System.Drawing.Point(402, 680)
        Me.LBCamera1.Name = "LBCamera1"
        Me.LBCamera1.Size = New System.Drawing.Size(52, 13)
        Me.LBCamera1.TabIndex = 137
        Me.LBCamera1.Text = "Camera 1"
        Me.LBCamera1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Camera1
        '
        Me.Camera1.Location = New System.Drawing.Point(457, 677)
        Me.Camera1.Name = "Camera1"
        Me.Camera1.Size = New System.Drawing.Size(236, 20)
        Me.Camera1.TabIndex = 810
        '
        'ButtonSave
        '
        Me.ButtonSave.BackColor = System.Drawing.Color.White
        Me.ButtonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.Blue
        Me.ButtonSave.Location = New System.Drawing.Point(924, 861)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(118, 23)
        Me.ButtonSave.TabIndex = 1000
        Me.ButtonSave.Text = "Save to Graphics"
        Me.ButtonSave.UseVisualStyleBackColor = False
        '
        'DB1
        '
        Me.DB1.AutoSize = True
        Me.DB1.Location = New System.Drawing.Point(15, 34)
        Me.DB1.Name = "DB1"
        Me.DB1.Size = New System.Drawing.Size(14, 13)
        Me.DB1.TabIndex = 2000
        Me.DB1.UseVisualStyleBackColor = True
        '
        'DB2
        '
        Me.DB2.AutoSize = True
        Me.DB2.Location = New System.Drawing.Point(362, 34)
        Me.DB2.Name = "DB2"
        Me.DB2.Size = New System.Drawing.Size(14, 13)
        Me.DB2.TabIndex = 2005
        Me.DB2.UseVisualStyleBackColor = True
        '
        'DB3
        '
        Me.DB3.AutoSize = True
        Me.DB3.Location = New System.Drawing.Point(711, 34)
        Me.DB3.Name = "DB3"
        Me.DB3.Size = New System.Drawing.Size(14, 13)
        Me.DB3.TabIndex = 2010
        Me.DB3.UseVisualStyleBackColor = True
        '
        'DB4
        '
        Me.DB4.AutoSize = True
        Me.DB4.Location = New System.Drawing.Point(15, 324)
        Me.DB4.Name = "DB4"
        Me.DB4.Size = New System.Drawing.Size(14, 13)
        Me.DB4.TabIndex = 2015
        Me.DB4.UseVisualStyleBackColor = True
        '
        'DB5
        '
        Me.DB5.AutoSize = True
        Me.DB5.Location = New System.Drawing.Point(362, 322)
        Me.DB5.Name = "DB5"
        Me.DB5.Size = New System.Drawing.Size(14, 13)
        Me.DB5.TabIndex = 2020
        Me.DB5.UseVisualStyleBackColor = True
        '
        'DB6
        '
        Me.DB6.AutoSize = True
        Me.DB6.Location = New System.Drawing.Point(711, 321)
        Me.DB6.Name = "DB6"
        Me.DB6.Size = New System.Drawing.Size(14, 13)
        Me.DB6.TabIndex = 2025
        Me.DB6.UseVisualStyleBackColor = True
        '
        'DBbox
        '
        Me.DBbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DBbox.FormattingEnabled = True
        Me.DBbox.Location = New System.Drawing.Point(110, 589)
        Me.DBbox.Name = "DBbox"
        Me.DBbox.Size = New System.Drawing.Size(236, 21)
        Me.DBbox.TabIndex = 2500
        Me.DBbox.TabStop = False
        '
        'DBload
        '
        Me.DBload.BackColor = System.Drawing.Color.White
        Me.DBload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DBload.ForeColor = System.Drawing.Color.Blue
        Me.DBload.Location = New System.Drawing.Point(355, 588)
        Me.DBload.Name = "DBload"
        Me.DBload.Size = New System.Drawing.Size(75, 23)
        Me.DBload.TabIndex = 1005
        Me.DBload.TabStop = False
        Me.DBload.Text = "Load"
        Me.DBload.UseVisualStyleBackColor = False
        '
        'Clear
        '
        Me.Clear.BackColor = System.Drawing.Color.White
        Me.Clear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clear.ForeColor = System.Drawing.Color.Blue
        Me.Clear.Location = New System.Drawing.Point(436, 588)
        Me.Clear.Name = "Clear"
        Me.Clear.Size = New System.Drawing.Size(75, 23)
        Me.Clear.TabIndex = 1010
        Me.Clear.TabStop = False
        Me.Clear.Text = "Clear"
        Me.Clear.UseVisualStyleBackColor = False
        '
        'SaveDB
        '
        Me.SaveDB.BackColor = System.Drawing.Color.White
        Me.SaveDB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveDB.ForeColor = System.Drawing.Color.Blue
        Me.SaveDB.Location = New System.Drawing.Point(517, 588)
        Me.SaveDB.Name = "SaveDB"
        Me.SaveDB.Size = New System.Drawing.Size(118, 23)
        Me.SaveDB.TabIndex = 1015
        Me.SaveDB.TabStop = False
        Me.SaveDB.Text = "Save to Database"
        Me.SaveDB.UseVisualStyleBackColor = False
        '
        'LBShowTitle
        '
        Me.LBShowTitle.AutoSize = True
        Me.LBShowTitle.Location = New System.Drawing.Point(747, 812)
        Me.LBShowTitle.Name = "LBShowTitle"
        Me.LBShowTitle.Size = New System.Drawing.Size(57, 13)
        Me.LBShowTitle.TabIndex = 173
        Me.LBShowTitle.Text = "Show Title"
        Me.LBShowTitle.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowTitle
        '
        Me.ShowTitle.Location = New System.Drawing.Point(806, 809)
        Me.ShowTitle.Name = "ShowTitle"
        Me.ShowTitle.Size = New System.Drawing.Size(236, 20)
        Me.ShowTitle.TabIndex = 940
        '
        'LBGuestDB
        '
        Me.LBGuestDB.AutoSize = True
        Me.LBGuestDB.Location = New System.Drawing.Point(20, 593)
        Me.LBGuestDB.Name = "LBGuestDB"
        Me.LBGuestDB.Size = New System.Drawing.Size(84, 13)
        Me.LBGuestDB.TabIndex = 169
        Me.LBGuestDB.Text = "Guest Database"
        Me.LBGuestDB.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CDBSave
        '
        Me.CDBSave.BackColor = System.Drawing.Color.White
        Me.CDBSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CDBSave.ForeColor = System.Drawing.Color.Blue
        Me.CDBSave.Location = New System.Drawing.Point(436, 826)
        Me.CDBSave.Name = "CDBSave"
        Me.CDBSave.Size = New System.Drawing.Size(118, 23)
        Me.CDBSave.TabIndex = 1025
        Me.CDBSave.TabStop = False
        Me.CDBSave.Text = "Save to Database"
        Me.CDBSave.UseVisualStyleBackColor = False
        '
        'CDBload
        '
        Me.CDBload.BackColor = System.Drawing.Color.White
        Me.CDBload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CDBload.ForeColor = System.Drawing.Color.Blue
        Me.CDBload.Location = New System.Drawing.Point(355, 826)
        Me.CDBload.Name = "CDBload"
        Me.CDBload.Size = New System.Drawing.Size(75, 23)
        Me.CDBload.TabIndex = 1020
        Me.CDBload.TabStop = False
        Me.CDBload.Text = "Load"
        Me.CDBload.UseVisualStyleBackColor = False
        '
        'LBCDB
        '
        Me.LBCDB.AutoSize = True
        Me.LBCDB.Location = New System.Drawing.Point(25, 831)
        Me.LBCDB.Name = "LBCDB"
        Me.LBCDB.Size = New System.Drawing.Size(80, 13)
        Me.LBCDB.TabIndex = 175
        Me.LBCDB.Text = "Crew Database"
        Me.LBCDB.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CDBbox
        '
        Me.CDBbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CDBbox.FormattingEnabled = True
        Me.CDBbox.Location = New System.Drawing.Point(110, 827)
        Me.CDBbox.Name = "CDBbox"
        Me.CDBbox.Size = New System.Drawing.Size(236, 21)
        Me.CDBbox.TabIndex = 2505
        Me.CDBbox.TabStop = False
        '
        'CDBProducer1
        '
        Me.CDBProducer1.AutoSize = True
        Me.CDBProducer1.Location = New System.Drawing.Point(32, 636)
        Me.CDBProducer1.Name = "CDBProducer1"
        Me.CDBProducer1.Size = New System.Drawing.Size(14, 13)
        Me.CDBProducer1.TabIndex = 2030
        Me.CDBProducer1.UseVisualStyleBackColor = True
        '
        'CDBHost1
        '
        Me.CDBHost1.AutoSize = True
        Me.CDBHost1.Location = New System.Drawing.Point(32, 680)
        Me.CDBHost1.Name = "CDBHost1"
        Me.CDBHost1.Size = New System.Drawing.Size(14, 13)
        Me.CDBHost1.TabIndex = 2040
        Me.CDBHost1.UseVisualStyleBackColor = True
        '
        'CDBDirector2
        '
        Me.CDBDirector2.AutoSize = True
        Me.CDBDirector2.Location = New System.Drawing.Point(32, 746)
        Me.CDBDirector2.Name = "CDBDirector2"
        Me.CDBDirector2.Size = New System.Drawing.Size(14, 13)
        Me.CDBDirector2.TabIndex = 2055
        Me.CDBDirector2.UseVisualStyleBackColor = True
        '
        'CDBDirector1
        '
        Me.CDBDirector1.AutoSize = True
        Me.CDBDirector1.Location = New System.Drawing.Point(32, 724)
        Me.CDBDirector1.Name = "CDBDirector1"
        Me.CDBDirector1.Size = New System.Drawing.Size(14, 13)
        Me.CDBDirector1.TabIndex = 2050
        Me.CDBDirector1.UseVisualStyleBackColor = True
        '
        'CDBGraphics2
        '
        Me.CDBGraphics2.AutoSize = True
        Me.CDBGraphics2.Location = New System.Drawing.Point(32, 790)
        Me.CDBGraphics2.Name = "CDBGraphics2"
        Me.CDBGraphics2.Size = New System.Drawing.Size(14, 13)
        Me.CDBGraphics2.TabIndex = 2065
        Me.CDBGraphics2.UseVisualStyleBackColor = True
        '
        'CDBGraphics1
        '
        Me.CDBGraphics1.AutoSize = True
        Me.CDBGraphics1.Location = New System.Drawing.Point(32, 768)
        Me.CDBGraphics1.Name = "CDBGraphics1"
        Me.CDBGraphics1.Size = New System.Drawing.Size(14, 13)
        Me.CDBGraphics1.TabIndex = 2060
        Me.CDBGraphics1.UseVisualStyleBackColor = True
        '
        'CDBCamera3
        '
        Me.CDBCamera3.AutoSize = True
        Me.CDBCamera3.Location = New System.Drawing.Point(386, 724)
        Me.CDBCamera3.Name = "CDBCamera3"
        Me.CDBCamera3.Size = New System.Drawing.Size(14, 13)
        Me.CDBCamera3.TabIndex = 2080
        Me.CDBCamera3.UseVisualStyleBackColor = True
        '
        'CDBCamera2
        '
        Me.CDBCamera2.AutoSize = True
        Me.CDBCamera2.Location = New System.Drawing.Point(386, 702)
        Me.CDBCamera2.Name = "CDBCamera2"
        Me.CDBCamera2.Size = New System.Drawing.Size(14, 13)
        Me.CDBCamera2.TabIndex = 2075
        Me.CDBCamera2.UseVisualStyleBackColor = True
        '
        'CDBCamera1
        '
        Me.CDBCamera1.AutoSize = True
        Me.CDBCamera1.Location = New System.Drawing.Point(386, 680)
        Me.CDBCamera1.Name = "CDBCamera1"
        Me.CDBCamera1.Size = New System.Drawing.Size(14, 13)
        Me.CDBCamera1.TabIndex = 2070
        Me.CDBCamera1.UseVisualStyleBackColor = True
        '
        'CDBAudio2
        '
        Me.CDBAudio2.AutoSize = True
        Me.CDBAudio2.Location = New System.Drawing.Point(386, 658)
        Me.CDBAudio2.Name = "CDBAudio2"
        Me.CDBAudio2.Size = New System.Drawing.Size(14, 13)
        Me.CDBAudio2.TabIndex = 2065
        Me.CDBAudio2.UseVisualStyleBackColor = True
        '
        'CDBAudio1
        '
        Me.CDBAudio1.AutoSize = True
        Me.CDBAudio1.Location = New System.Drawing.Point(386, 636)
        Me.CDBAudio1.Name = "CDBAudio1"
        Me.CDBAudio1.Size = New System.Drawing.Size(14, 13)
        Me.CDBAudio1.TabIndex = 2060
        Me.CDBAudio1.UseVisualStyleBackColor = True
        '
        'TextFileCheckTime
        '
        Me.TextFileCheckTime.Enabled = True
        Me.TextFileCheckTime.Interval = 15000
        '
        'LBShowTwitter
        '
        Me.LBShowTwitter.AutoSize = True
        Me.LBShowTwitter.Location = New System.Drawing.Point(735, 746)
        Me.LBShowTwitter.Name = "LBShowTwitter"
        Me.LBShowTwitter.Size = New System.Drawing.Size(69, 13)
        Me.LBShowTwitter.TabIndex = 190
        Me.LBShowTwitter.Text = "Show Twitter"
        Me.LBShowTwitter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowTwitter
        '
        Me.ShowTwitter.Location = New System.Drawing.Point(806, 743)
        Me.ShowTwitter.Name = "ShowTwitter"
        Me.ShowTwitter.Size = New System.Drawing.Size(236, 20)
        Me.ShowTwitter.TabIndex = 925
        '
        'LBShowFacebook
        '
        Me.LBShowFacebook.AutoSize = True
        Me.LBShowFacebook.Location = New System.Drawing.Point(718, 724)
        Me.LBShowFacebook.Name = "LBShowFacebook"
        Me.LBShowFacebook.Size = New System.Drawing.Size(85, 13)
        Me.LBShowFacebook.TabIndex = 192
        Me.LBShowFacebook.Text = "Show Facebook"
        Me.LBShowFacebook.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowFacebook
        '
        Me.ShowFacebook.Location = New System.Drawing.Point(806, 721)
        Me.ShowFacebook.Name = "ShowFacebook"
        Me.ShowFacebook.Size = New System.Drawing.Size(236, 20)
        Me.ShowFacebook.TabIndex = 920
        '
        'LBShowWebsite
        '
        Me.LBShowWebsite.AutoSize = True
        Me.LBShowWebsite.Location = New System.Drawing.Point(728, 702)
        Me.LBShowWebsite.Name = "LBShowWebsite"
        Me.LBShowWebsite.Size = New System.Drawing.Size(76, 13)
        Me.LBShowWebsite.TabIndex = 194
        Me.LBShowWebsite.Text = "Show Website"
        Me.LBShowWebsite.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowWebsite
        '
        Me.ShowWebsite.Location = New System.Drawing.Point(806, 699)
        Me.ShowWebsite.Name = "ShowWebsite"
        Me.ShowWebsite.Size = New System.Drawing.Size(236, 20)
        Me.ShowWebsite.TabIndex = 915
        '
        'LBShowName
        '
        Me.LBShowName.AutoSize = True
        Me.LBShowName.Location = New System.Drawing.Point(739, 835)
        Me.LBShowName.Name = "LBShowName"
        Me.LBShowName.Size = New System.Drawing.Size(65, 13)
        Me.LBShowName.TabIndex = 201
        Me.LBShowName.Text = "Show Name"
        Me.LBShowName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ShowName
        '
        Me.ShowName.Location = New System.Drawing.Point(806, 831)
        Me.ShowName.Name = "ShowName"
        Me.ShowName.Size = New System.Drawing.Size(236, 20)
        Me.ShowName.TabIndex = 945
        '
        'LBShowemail
        '
        Me.LBShowemail.AutoSize = True
        Me.LBShowemail.Location = New System.Drawing.Point(743, 680)
        Me.LBShowemail.Name = "LBShowemail"
        Me.LBShowemail.Size = New System.Drawing.Size(61, 13)
        Me.LBShowemail.TabIndex = 302
        Me.LBShowemail.Text = "Show email"
        Me.LBShowemail.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Showemail
        '
        Me.Showemail.Location = New System.Drawing.Point(806, 677)
        Me.Showemail.Name = "Showemail"
        Me.Showemail.Size = New System.Drawing.Size(236, 20)
        Me.Showemail.TabIndex = 910
        '
        'CDBFloor3
        '
        Me.CDBFloor3.AutoSize = True
        Me.CDBFloor3.Location = New System.Drawing.Point(386, 790)
        Me.CDBFloor3.Name = "CDBFloor3"
        Me.CDBFloor3.Size = New System.Drawing.Size(14, 13)
        Me.CDBFloor3.TabIndex = 2514
        Me.CDBFloor3.UseVisualStyleBackColor = True
        '
        'CDBFloor2
        '
        Me.CDBFloor2.AutoSize = True
        Me.CDBFloor2.Location = New System.Drawing.Point(386, 768)
        Me.CDBFloor2.Name = "CDBFloor2"
        Me.CDBFloor2.Size = New System.Drawing.Size(14, 13)
        Me.CDBFloor2.TabIndex = 2513
        Me.CDBFloor2.UseVisualStyleBackColor = True
        '
        'CDBFloor1
        '
        Me.CDBFloor1.AutoSize = True
        Me.CDBFloor1.Location = New System.Drawing.Point(386, 746)
        Me.CDBFloor1.Name = "CDBFloor1"
        Me.CDBFloor1.Size = New System.Drawing.Size(14, 13)
        Me.CDBFloor1.TabIndex = 2512
        Me.CDBFloor1.UseVisualStyleBackColor = True
        '
        'LBFloor3
        '
        Me.LBFloor3.AutoSize = True
        Me.LBFloor3.Location = New System.Drawing.Point(415, 790)
        Me.LBFloor3.Name = "LBFloor3"
        Me.LBFloor3.Size = New System.Drawing.Size(39, 13)
        Me.LBFloor3.TabIndex = 2508
        Me.LBFloor3.Text = "Floor 3"
        Me.LBFloor3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Floor3
        '
        Me.Floor3.Location = New System.Drawing.Point(457, 787)
        Me.Floor3.Name = "Floor3"
        Me.Floor3.Size = New System.Drawing.Size(236, 20)
        Me.Floor3.TabIndex = 835
        '
        'LBFloor2
        '
        Me.LBFloor2.AutoSize = True
        Me.LBFloor2.Location = New System.Drawing.Point(415, 768)
        Me.LBFloor2.Name = "LBFloor2"
        Me.LBFloor2.Size = New System.Drawing.Size(39, 13)
        Me.LBFloor2.TabIndex = 2507
        Me.LBFloor2.Text = "Floor 2"
        Me.LBFloor2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Floor2
        '
        Me.Floor2.Location = New System.Drawing.Point(457, 765)
        Me.Floor2.Name = "Floor2"
        Me.Floor2.Size = New System.Drawing.Size(236, 20)
        Me.Floor2.TabIndex = 830
        '
        'LBFloor1
        '
        Me.LBFloor1.AutoSize = True
        Me.LBFloor1.Location = New System.Drawing.Point(415, 746)
        Me.LBFloor1.Name = "LBFloor1"
        Me.LBFloor1.Size = New System.Drawing.Size(39, 13)
        Me.LBFloor1.TabIndex = 2506
        Me.LBFloor1.Text = "Floor 1"
        Me.LBFloor1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Floor1
        '
        Me.Floor1.Location = New System.Drawing.Point(457, 743)
        Me.Floor1.Name = "Floor1"
        Me.Floor1.Size = New System.Drawing.Size(236, 20)
        Me.Floor1.TabIndex = 825
        '
        'CDBProducer2
        '
        Me.CDBProducer2.AutoSize = True
        Me.CDBProducer2.Location = New System.Drawing.Point(32, 658)
        Me.CDBProducer2.Name = "CDBProducer2"
        Me.CDBProducer2.Size = New System.Drawing.Size(14, 13)
        Me.CDBProducer2.TabIndex = 2035
        Me.CDBProducer2.UseVisualStyleBackColor = True
        '
        'LBProducer2
        '
        Me.LBProducer2.AutoSize = True
        Me.LBProducer2.Location = New System.Drawing.Point(48, 658)
        Me.LBProducer2.Name = "LBProducer2"
        Me.LBProducer2.Size = New System.Drawing.Size(59, 13)
        Me.LBProducer2.TabIndex = 2515
        Me.LBProducer2.Text = "Producer 2"
        Me.LBProducer2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Producer2
        '
        Me.Producer2.Location = New System.Drawing.Point(110, 655)
        Me.Producer2.Name = "Producer2"
        Me.Producer2.Size = New System.Drawing.Size(236, 20)
        Me.Producer2.TabIndex = 705
        '
        'CDBHost2
        '
        Me.CDBHost2.AutoSize = True
        Me.CDBHost2.Location = New System.Drawing.Point(32, 702)
        Me.CDBHost2.Name = "CDBHost2"
        Me.CDBHost2.Size = New System.Drawing.Size(14, 13)
        Me.CDBHost2.TabIndex = 2045
        Me.CDBHost2.UseVisualStyleBackColor = True
        '
        'LBHost2
        '
        Me.LBHost2.AutoSize = True
        Me.LBHost2.Location = New System.Drawing.Point(69, 702)
        Me.LBHost2.Name = "LBHost2"
        Me.LBHost2.Size = New System.Drawing.Size(38, 13)
        Me.LBHost2.TabIndex = 2518
        Me.LBHost2.Text = "Host 2"
        Me.LBHost2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Host2
        '
        Me.Host2.Location = New System.Drawing.Point(110, 699)
        Me.Host2.Name = "Host2"
        Me.Host2.Size = New System.Drawing.Size(236, 20)
        Me.Host2.TabIndex = 715
        '
        'LBTwitter3
        '
        Me.LBTwitter3.AutoSize = True
        Me.LBTwitter3.Location = New System.Drawing.Point(757, 147)
        Me.LBTwitter3.Name = "LBTwitter3"
        Me.LBTwitter3.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter3.TabIndex = 2524
        Me.LBTwitter3.Text = "Twitter 3"
        '
        'Twitter3
        '
        Me.Twitter3.Location = New System.Drawing.Point(806, 144)
        Me.Twitter3.Name = "Twitter3"
        Me.Twitter3.Size = New System.Drawing.Size(236, 20)
        Me.Twitter3.TabIndex = 330
        '
        'LBFacebook3
        '
        Me.LBFacebook3.AutoSize = True
        Me.LBFacebook3.Location = New System.Drawing.Point(741, 125)
        Me.LBFacebook3.Name = "LBFacebook3"
        Me.LBFacebook3.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook3.TabIndex = 2523
        Me.LBFacebook3.Text = "Facebook 3"
        '
        'Facebook3
        '
        Me.Facebook3.Location = New System.Drawing.Point(806, 122)
        Me.Facebook3.Name = "Facebook3"
        Me.Facebook3.Size = New System.Drawing.Size(236, 20)
        Me.Facebook3.TabIndex = 325
        '
        'LBTwitter2
        '
        Me.LBTwitter2.AutoSize = True
        Me.LBTwitter2.Location = New System.Drawing.Point(408, 147)
        Me.LBTwitter2.Name = "LBTwitter2"
        Me.LBTwitter2.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter2.TabIndex = 2522
        Me.LBTwitter2.Text = "Twitter 2"
        '
        'Twitter2
        '
        Me.Twitter2.Location = New System.Drawing.Point(457, 144)
        Me.Twitter2.Name = "Twitter2"
        Me.Twitter2.Size = New System.Drawing.Size(236, 20)
        Me.Twitter2.TabIndex = 230
        '
        'LBFacebook2
        '
        Me.LBFacebook2.AutoSize = True
        Me.LBFacebook2.Location = New System.Drawing.Point(392, 125)
        Me.LBFacebook2.Name = "LBFacebook2"
        Me.LBFacebook2.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook2.TabIndex = 2521
        Me.LBFacebook2.Text = "Facebook 2"
        '
        'Facebook2
        '
        Me.Facebook2.Location = New System.Drawing.Point(457, 122)
        Me.Facebook2.Name = "Facebook2"
        Me.Facebook2.Size = New System.Drawing.Size(236, 20)
        Me.Facebook2.TabIndex = 225
        '
        'LBTwitter1
        '
        Me.LBTwitter1.AutoSize = True
        Me.LBTwitter1.Location = New System.Drawing.Point(61, 147)
        Me.LBTwitter1.Name = "LBTwitter1"
        Me.LBTwitter1.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter1.TabIndex = 2520
        Me.LBTwitter1.Text = "Twitter 1"
        '
        'Twitter1
        '
        Me.Twitter1.Location = New System.Drawing.Point(110, 144)
        Me.Twitter1.Name = "Twitter1"
        Me.Twitter1.Size = New System.Drawing.Size(236, 20)
        Me.Twitter1.TabIndex = 130
        '
        'LBFacebook1
        '
        Me.LBFacebook1.AutoSize = True
        Me.LBFacebook1.Location = New System.Drawing.Point(45, 125)
        Me.LBFacebook1.Name = "LBFacebook1"
        Me.LBFacebook1.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook1.TabIndex = 2519
        Me.LBFacebook1.Text = "Facebook 1"
        Me.LBFacebook1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Facebook1
        '
        Me.Facebook1.Location = New System.Drawing.Point(110, 122)
        Me.Facebook1.Name = "Facebook1"
        Me.Facebook1.Size = New System.Drawing.Size(236, 20)
        Me.Facebook1.TabIndex = 125
        '
        'LBTwitter6
        '
        Me.LBTwitter6.AutoSize = True
        Me.LBTwitter6.Location = New System.Drawing.Point(757, 438)
        Me.LBTwitter6.Name = "LBTwitter6"
        Me.LBTwitter6.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter6.TabIndex = 2536
        Me.LBTwitter6.Text = "Twitter 6"
        '
        'Twitter6
        '
        Me.Twitter6.Location = New System.Drawing.Point(806, 435)
        Me.Twitter6.Name = "Twitter6"
        Me.Twitter6.Size = New System.Drawing.Size(236, 20)
        Me.Twitter6.TabIndex = 630
        '
        'LBFacebook6
        '
        Me.LBFacebook6.AutoSize = True
        Me.LBFacebook6.Location = New System.Drawing.Point(741, 416)
        Me.LBFacebook6.Name = "LBFacebook6"
        Me.LBFacebook6.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook6.TabIndex = 2535
        Me.LBFacebook6.Text = "Facebook 6"
        '
        'Facebook6
        '
        Me.Facebook6.Location = New System.Drawing.Point(806, 413)
        Me.Facebook6.Name = "Facebook6"
        Me.Facebook6.Size = New System.Drawing.Size(236, 20)
        Me.Facebook6.TabIndex = 625
        '
        'LBTwitter5
        '
        Me.LBTwitter5.AutoSize = True
        Me.LBTwitter5.Location = New System.Drawing.Point(408, 438)
        Me.LBTwitter5.Name = "LBTwitter5"
        Me.LBTwitter5.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter5.TabIndex = 2534
        Me.LBTwitter5.Text = "Twitter 5"
        '
        'Twitter5
        '
        Me.Twitter5.Location = New System.Drawing.Point(457, 435)
        Me.Twitter5.Name = "Twitter5"
        Me.Twitter5.Size = New System.Drawing.Size(236, 20)
        Me.Twitter5.TabIndex = 530
        '
        'LBFacebook5
        '
        Me.LBFacebook5.AutoSize = True
        Me.LBFacebook5.Location = New System.Drawing.Point(392, 416)
        Me.LBFacebook5.Name = "LBFacebook5"
        Me.LBFacebook5.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook5.TabIndex = 2533
        Me.LBFacebook5.Text = "Facebook 5"
        '
        'Facebook5
        '
        Me.Facebook5.Location = New System.Drawing.Point(457, 413)
        Me.Facebook5.Name = "Facebook5"
        Me.Facebook5.Size = New System.Drawing.Size(236, 20)
        Me.Facebook5.TabIndex = 525
        '
        'LBTwitter4
        '
        Me.LBTwitter4.AutoSize = True
        Me.LBTwitter4.Location = New System.Drawing.Point(61, 438)
        Me.LBTwitter4.Name = "LBTwitter4"
        Me.LBTwitter4.Size = New System.Drawing.Size(48, 13)
        Me.LBTwitter4.TabIndex = 2532
        Me.LBTwitter4.Text = "Twitter 4"
        '
        'Twitter4
        '
        Me.Twitter4.Location = New System.Drawing.Point(110, 435)
        Me.Twitter4.Name = "Twitter4"
        Me.Twitter4.Size = New System.Drawing.Size(236, 20)
        Me.Twitter4.TabIndex = 430
        '
        'LBFacebook4
        '
        Me.LBFacebook4.AutoSize = True
        Me.LBFacebook4.Location = New System.Drawing.Point(45, 416)
        Me.LBFacebook4.Name = "LBFacebook4"
        Me.LBFacebook4.Size = New System.Drawing.Size(64, 13)
        Me.LBFacebook4.TabIndex = 2531
        Me.LBFacebook4.Text = "Facebook 4"
        Me.LBFacebook4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Facebook4
        '
        Me.Facebook4.Location = New System.Drawing.Point(110, 413)
        Me.Facebook4.Name = "Facebook4"
        Me.Facebook4.Size = New System.Drawing.Size(236, 20)
        Me.Facebook4.TabIndex = 425
        '
        'LBYouTube3
        '
        Me.LBYouTube3.AutoSize = True
        Me.LBYouTube3.Location = New System.Drawing.Point(745, 169)
        Me.LBYouTube3.Name = "LBYouTube3"
        Me.LBYouTube3.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube3.TabIndex = 2542
        Me.LBYouTube3.Text = "YouTube 3"
        '
        'YouTube3
        '
        Me.YouTube3.Location = New System.Drawing.Point(806, 166)
        Me.YouTube3.Name = "YouTube3"
        Me.YouTube3.Size = New System.Drawing.Size(236, 20)
        Me.YouTube3.TabIndex = 335
        '
        'LBYouTube2
        '
        Me.LBYouTube2.AutoSize = True
        Me.LBYouTube2.Location = New System.Drawing.Point(396, 169)
        Me.LBYouTube2.Name = "LBYouTube2"
        Me.LBYouTube2.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube2.TabIndex = 2541
        Me.LBYouTube2.Text = "YouTube 2"
        '
        'YouTube2
        '
        Me.YouTube2.Location = New System.Drawing.Point(457, 166)
        Me.YouTube2.Name = "YouTube2"
        Me.YouTube2.Size = New System.Drawing.Size(236, 20)
        Me.YouTube2.TabIndex = 235
        '
        'LBYouTube1
        '
        Me.LBYouTube1.AutoSize = True
        Me.LBYouTube1.Location = New System.Drawing.Point(49, 169)
        Me.LBYouTube1.Name = "LBYouTube1"
        Me.LBYouTube1.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube1.TabIndex = 2540
        Me.LBYouTube1.Text = "YouTube 1"
        '
        'YouTube1
        '
        Me.YouTube1.Location = New System.Drawing.Point(110, 166)
        Me.YouTube1.Name = "YouTube1"
        Me.YouTube1.Size = New System.Drawing.Size(236, 20)
        Me.YouTube1.TabIndex = 135
        '
        'LBYouTube6
        '
        Me.LBYouTube6.AutoSize = True
        Me.LBYouTube6.Location = New System.Drawing.Point(745, 460)
        Me.LBYouTube6.Name = "LBYouTube6"
        Me.LBYouTube6.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube6.TabIndex = 2548
        Me.LBYouTube6.Text = "YouTube 6"
        '
        'YouTube6
        '
        Me.YouTube6.Location = New System.Drawing.Point(806, 457)
        Me.YouTube6.Name = "YouTube6"
        Me.YouTube6.Size = New System.Drawing.Size(236, 20)
        Me.YouTube6.TabIndex = 635
        '
        'LBYouTube5
        '
        Me.LBYouTube5.AutoSize = True
        Me.LBYouTube5.Location = New System.Drawing.Point(396, 460)
        Me.LBYouTube5.Name = "LBYouTube5"
        Me.LBYouTube5.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube5.TabIndex = 2547
        Me.LBYouTube5.Text = "YouTube 5"
        '
        'YouTube5
        '
        Me.YouTube5.Location = New System.Drawing.Point(457, 457)
        Me.YouTube5.Name = "YouTube5"
        Me.YouTube5.Size = New System.Drawing.Size(236, 20)
        Me.YouTube5.TabIndex = 535
        '
        'LBYouTube4
        '
        Me.LBYouTube4.AutoSize = True
        Me.LBYouTube4.Location = New System.Drawing.Point(49, 460)
        Me.LBYouTube4.Name = "LBYouTube4"
        Me.LBYouTube4.Size = New System.Drawing.Size(60, 13)
        Me.LBYouTube4.TabIndex = 2546
        Me.LBYouTube4.Text = "YouTube 4"
        '
        'YouTube4
        '
        Me.YouTube4.Location = New System.Drawing.Point(110, 457)
        Me.YouTube4.Name = "YouTube4"
        Me.YouTube4.Size = New System.Drawing.Size(236, 20)
        Me.YouTube4.TabIndex = 435
        '
        'ButtonSettings
        '
        Me.ButtonSettings.BackColor = System.Drawing.Color.White
        Me.ButtonSettings.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSettings.ForeColor = System.Drawing.Color.Blue
        Me.ButtonSettings.Location = New System.Drawing.Point(12, 861)
        Me.ButtonSettings.Name = "ButtonSettings"
        Me.ButtonSettings.Size = New System.Drawing.Size(118, 23)
        Me.ButtonSettings.TabIndex = 2549
        Me.ButtonSettings.TabStop = False
        Me.ButtonSettings.Text = "Settings"
        Me.ButtonSettings.UseVisualStyleBackColor = False
        '
        'LBOfficeemail6
        '
        Me.LBOfficeemail6.AutoSize = True
        Me.LBOfficeemail6.Location = New System.Drawing.Point(734, 548)
        Me.LBOfficeemail6.Name = "LBOfficeemail6"
        Me.LBOfficeemail6.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail6.TabIndex = 2555
        Me.LBOfficeemail6.Text = "Office email 6"
        '
        'Officeemail6
        '
        Me.Officeemail6.Location = New System.Drawing.Point(806, 545)
        Me.Officeemail6.Name = "Officeemail6"
        Me.Officeemail6.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail6.TabIndex = 2561
        '
        'LBOfficePhone6
        '
        Me.LBOfficePhone6.AutoSize = True
        Me.LBOfficePhone6.Location = New System.Drawing.Point(727, 527)
        Me.LBOfficePhone6.Name = "LBOfficePhone6"
        Me.LBOfficePhone6.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone6.TabIndex = 2554
        Me.LBOfficePhone6.Text = "Office Phone 6"
        '
        'OfficePhone6
        '
        Me.OfficePhone6.Location = New System.Drawing.Point(806, 523)
        Me.OfficePhone6.Name = "OfficePhone6"
        Me.OfficePhone6.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone6.TabIndex = 2560
        '
        'LBOfficeemail5
        '
        Me.LBOfficeemail5.AutoSize = True
        Me.LBOfficeemail5.Location = New System.Drawing.Point(385, 547)
        Me.LBOfficeemail5.Name = "LBOfficeemail5"
        Me.LBOfficeemail5.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail5.TabIndex = 2553
        Me.LBOfficeemail5.Text = "Office email 5"
        '
        'Officeemail5
        '
        Me.Officeemail5.Location = New System.Drawing.Point(457, 545)
        Me.Officeemail5.Name = "Officeemail5"
        Me.Officeemail5.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail5.TabIndex = 2559
        '
        'LBOfficePhone5
        '
        Me.LBOfficePhone5.AutoSize = True
        Me.LBOfficePhone5.Location = New System.Drawing.Point(378, 526)
        Me.LBOfficePhone5.Name = "LBOfficePhone5"
        Me.LBOfficePhone5.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone5.TabIndex = 2552
        Me.LBOfficePhone5.Text = "Office Phone 5"
        '
        'OfficePhone5
        '
        Me.OfficePhone5.Location = New System.Drawing.Point(457, 523)
        Me.OfficePhone5.Name = "OfficePhone5"
        Me.OfficePhone5.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone5.TabIndex = 2558
        '
        'LBOfficeemail4
        '
        Me.LBOfficeemail4.AutoSize = True
        Me.LBOfficeemail4.Location = New System.Drawing.Point(38, 548)
        Me.LBOfficeemail4.Name = "LBOfficeemail4"
        Me.LBOfficeemail4.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail4.TabIndex = 2551
        Me.LBOfficeemail4.Text = "Office email 4"
        '
        'Officeemail4
        '
        Me.Officeemail4.Location = New System.Drawing.Point(110, 545)
        Me.Officeemail4.Name = "Officeemail4"
        Me.Officeemail4.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail4.TabIndex = 2557
        '
        'LBOfficePhone4
        '
        Me.LBOfficePhone4.AutoSize = True
        Me.LBOfficePhone4.Location = New System.Drawing.Point(31, 526)
        Me.LBOfficePhone4.Name = "LBOfficePhone4"
        Me.LBOfficePhone4.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone4.TabIndex = 2550
        Me.LBOfficePhone4.Text = "Office Phone 4"
        '
        'OfficePhone4
        '
        Me.OfficePhone4.Location = New System.Drawing.Point(110, 523)
        Me.OfficePhone4.Name = "OfficePhone4"
        Me.OfficePhone4.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone4.TabIndex = 2556
        '
        'LBOfficeemail3
        '
        Me.LBOfficeemail3.AutoSize = True
        Me.LBOfficeemail3.Location = New System.Drawing.Point(734, 257)
        Me.LBOfficeemail3.Name = "LBOfficeemail3"
        Me.LBOfficeemail3.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail3.TabIndex = 2567
        Me.LBOfficeemail3.Text = "Office email 3"
        '
        'Officeemail3
        '
        Me.Officeemail3.Location = New System.Drawing.Point(806, 254)
        Me.Officeemail3.Name = "Officeemail3"
        Me.Officeemail3.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail3.TabIndex = 2573
        '
        'LBOfficePhone3
        '
        Me.LBOfficePhone3.AutoSize = True
        Me.LBOfficePhone3.Location = New System.Drawing.Point(727, 236)
        Me.LBOfficePhone3.Name = "LBOfficePhone3"
        Me.LBOfficePhone3.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone3.TabIndex = 2566
        Me.LBOfficePhone3.Text = "Office Phone 3"
        '
        'OfficePhone3
        '
        Me.OfficePhone3.Location = New System.Drawing.Point(806, 232)
        Me.OfficePhone3.Name = "OfficePhone3"
        Me.OfficePhone3.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone3.TabIndex = 2572
        '
        'LBOfficeemail2
        '
        Me.LBOfficeemail2.AutoSize = True
        Me.LBOfficeemail2.Location = New System.Drawing.Point(385, 256)
        Me.LBOfficeemail2.Name = "LBOfficeemail2"
        Me.LBOfficeemail2.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail2.TabIndex = 2565
        Me.LBOfficeemail2.Text = "Office email 2"
        '
        'Officeemail2
        '
        Me.Officeemail2.Location = New System.Drawing.Point(457, 254)
        Me.Officeemail2.Name = "Officeemail2"
        Me.Officeemail2.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail2.TabIndex = 2571
        '
        'LBOfficePhone2
        '
        Me.LBOfficePhone2.AutoSize = True
        Me.LBOfficePhone2.Location = New System.Drawing.Point(378, 235)
        Me.LBOfficePhone2.Name = "LBOfficePhone2"
        Me.LBOfficePhone2.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone2.TabIndex = 2564
        Me.LBOfficePhone2.Text = "Office Phone 2"
        '
        'OfficePhone2
        '
        Me.OfficePhone2.Location = New System.Drawing.Point(457, 232)
        Me.OfficePhone2.Name = "OfficePhone2"
        Me.OfficePhone2.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone2.TabIndex = 2570
        '
        'LBOfficeemail1
        '
        Me.LBOfficeemail1.AutoSize = True
        Me.LBOfficeemail1.Location = New System.Drawing.Point(38, 257)
        Me.LBOfficeemail1.Name = "LBOfficeemail1"
        Me.LBOfficeemail1.Size = New System.Drawing.Size(71, 13)
        Me.LBOfficeemail1.TabIndex = 2563
        Me.LBOfficeemail1.Text = "Office email 1"
        '
        'Officeemail1
        '
        Me.Officeemail1.Location = New System.Drawing.Point(110, 254)
        Me.Officeemail1.Name = "Officeemail1"
        Me.Officeemail1.Size = New System.Drawing.Size(236, 20)
        Me.Officeemail1.TabIndex = 2569
        '
        'LBOfficePhone1
        '
        Me.LBOfficePhone1.AutoSize = True
        Me.LBOfficePhone1.Location = New System.Drawing.Point(31, 235)
        Me.LBOfficePhone1.Name = "LBOfficePhone1"
        Me.LBOfficePhone1.Size = New System.Drawing.Size(78, 13)
        Me.LBOfficePhone1.TabIndex = 2562
        Me.LBOfficePhone1.Text = "Office Phone 1"
        '
        'OfficePhone1
        '
        Me.OfficePhone1.Location = New System.Drawing.Point(110, 232)
        Me.OfficePhone1.Name = "OfficePhone1"
        Me.OfficePhone1.Size = New System.Drawing.Size(236, 20)
        Me.OfficePhone1.TabIndex = 2568
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1055, 911)
        Me.Controls.Add(Me.LBOfficeemail3)
        Me.Controls.Add(Me.Officeemail3)
        Me.Controls.Add(Me.LBOfficePhone3)
        Me.Controls.Add(Me.OfficePhone3)
        Me.Controls.Add(Me.LBOfficeemail2)
        Me.Controls.Add(Me.Officeemail2)
        Me.Controls.Add(Me.LBOfficePhone2)
        Me.Controls.Add(Me.OfficePhone2)
        Me.Controls.Add(Me.LBOfficeemail1)
        Me.Controls.Add(Me.Officeemail1)
        Me.Controls.Add(Me.LBOfficePhone1)
        Me.Controls.Add(Me.OfficePhone1)
        Me.Controls.Add(Me.LBOfficeemail6)
        Me.Controls.Add(Me.Officeemail6)
        Me.Controls.Add(Me.LBOfficePhone6)
        Me.Controls.Add(Me.OfficePhone6)
        Me.Controls.Add(Me.LBOfficeemail5)
        Me.Controls.Add(Me.Officeemail5)
        Me.Controls.Add(Me.LBOfficePhone5)
        Me.Controls.Add(Me.OfficePhone5)
        Me.Controls.Add(Me.LBOfficeemail4)
        Me.Controls.Add(Me.Officeemail4)
        Me.Controls.Add(Me.LBOfficePhone4)
        Me.Controls.Add(Me.OfficePhone4)
        Me.Controls.Add(Me.ButtonSettings)
        Me.Controls.Add(Me.LBYouTube6)
        Me.Controls.Add(Me.YouTube6)
        Me.Controls.Add(Me.LBYouTube5)
        Me.Controls.Add(Me.YouTube5)
        Me.Controls.Add(Me.LBYouTube4)
        Me.Controls.Add(Me.YouTube4)
        Me.Controls.Add(Me.LBYouTube3)
        Me.Controls.Add(Me.YouTube3)
        Me.Controls.Add(Me.LBYouTube2)
        Me.Controls.Add(Me.YouTube2)
        Me.Controls.Add(Me.LBYouTube1)
        Me.Controls.Add(Me.YouTube1)
        Me.Controls.Add(Me.LBTwitter6)
        Me.Controls.Add(Me.Twitter6)
        Me.Controls.Add(Me.LBFacebook6)
        Me.Controls.Add(Me.Facebook6)
        Me.Controls.Add(Me.LBTwitter5)
        Me.Controls.Add(Me.Twitter5)
        Me.Controls.Add(Me.LBFacebook5)
        Me.Controls.Add(Me.Facebook5)
        Me.Controls.Add(Me.LBTwitter4)
        Me.Controls.Add(Me.Twitter4)
        Me.Controls.Add(Me.LBFacebook4)
        Me.Controls.Add(Me.Facebook4)
        Me.Controls.Add(Me.LBTwitter3)
        Me.Controls.Add(Me.Twitter3)
        Me.Controls.Add(Me.LBFacebook3)
        Me.Controls.Add(Me.Facebook3)
        Me.Controls.Add(Me.LBTwitter2)
        Me.Controls.Add(Me.Twitter2)
        Me.Controls.Add(Me.LBFacebook2)
        Me.Controls.Add(Me.Facebook2)
        Me.Controls.Add(Me.LBTwitter1)
        Me.Controls.Add(Me.Twitter1)
        Me.Controls.Add(Me.LBFacebook1)
        Me.Controls.Add(Me.Facebook1)
        Me.Controls.Add(Me.CDBHost2)
        Me.Controls.Add(Me.LBHost2)
        Me.Controls.Add(Me.Host2)
        Me.Controls.Add(Me.CDBProducer2)
        Me.Controls.Add(Me.LBProducer2)
        Me.Controls.Add(Me.Producer2)
        Me.Controls.Add(Me.CDBFloor3)
        Me.Controls.Add(Me.CDBFloor2)
        Me.Controls.Add(Me.CDBFloor1)
        Me.Controls.Add(Me.LBFloor3)
        Me.Controls.Add(Me.Floor3)
        Me.Controls.Add(Me.LBFloor2)
        Me.Controls.Add(Me.Floor2)
        Me.Controls.Add(Me.LBFloor1)
        Me.Controls.Add(Me.Floor1)
        Me.Controls.Add(Me.LBShowemail)
        Me.Controls.Add(Me.Showemail)
        Me.Controls.Add(Me.LBShowName)
        Me.Controls.Add(Me.ShowName)
        Me.Controls.Add(Me.LBShowWebsite)
        Me.Controls.Add(Me.ShowWebsite)
        Me.Controls.Add(Me.LBShowFacebook)
        Me.Controls.Add(Me.ShowFacebook)
        Me.Controls.Add(Me.LBShowTwitter)
        Me.Controls.Add(Me.ShowTwitter)
        Me.Controls.Add(Me.CDBCamera3)
        Me.Controls.Add(Me.CDBCamera2)
        Me.Controls.Add(Me.CDBCamera1)
        Me.Controls.Add(Me.CDBAudio2)
        Me.Controls.Add(Me.CDBAudio1)
        Me.Controls.Add(Me.CDBGraphics2)
        Me.Controls.Add(Me.CDBGraphics1)
        Me.Controls.Add(Me.CDBDirector2)
        Me.Controls.Add(Me.CDBDirector1)
        Me.Controls.Add(Me.CDBHost1)
        Me.Controls.Add(Me.CDBProducer1)
        Me.Controls.Add(Me.CDBSave)
        Me.Controls.Add(Me.CDBload)
        Me.Controls.Add(Me.LBCDB)
        Me.Controls.Add(Me.CDBbox)
        Me.Controls.Add(Me.LBShowTitle)
        Me.Controls.Add(Me.ShowTitle)
        Me.Controls.Add(Me.SaveDB)
        Me.Controls.Add(Me.Clear)
        Me.Controls.Add(Me.DBload)
        Me.Controls.Add(Me.LBGuestDB)
        Me.Controls.Add(Me.DBbox)
        Me.Controls.Add(Me.DB6)
        Me.Controls.Add(Me.DB5)
        Me.Controls.Add(Me.DB4)
        Me.Controls.Add(Me.DB3)
        Me.Controls.Add(Me.DB2)
        Me.Controls.Add(Me.DB1)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.LBCamera3)
        Me.Controls.Add(Me.Camera3)
        Me.Controls.Add(Me.LBCamera2)
        Me.Controls.Add(Me.Camera2)
        Me.Controls.Add(Me.LBCamera1)
        Me.Controls.Add(Me.Camera1)
        Me.Controls.Add(Me.LBAudio2)
        Me.Controls.Add(Me.Audio2)
        Me.Controls.Add(Me.LBAudio1)
        Me.Controls.Add(Me.Audio1)
        Me.Controls.Add(Me.LBGraphics2)
        Me.Controls.Add(Me.Graphics2)
        Me.Controls.Add(Me.LBGraphics1)
        Me.Controls.Add(Me.Graphics1)
        Me.Controls.Add(Me.LBDirector2)
        Me.Controls.Add(Me.Director2)
        Me.Controls.Add(Me.LBDirector1)
        Me.Controls.Add(Me.Director1)
        Me.Controls.Add(Me.LBHost1)
        Me.Controls.Add(Me.Host1)
        Me.Controls.Add(Me.LBProducer1)
        Me.Controls.Add(Me.Producer1)
        Me.Controls.Add(Me.LBRecordDate)
        Me.Controls.Add(Me.RecordDate)
        Me.Controls.Add(Me.LBShowYouTube)
        Me.Controls.Add(Me.ShowYouTube)
        Me.Controls.Add(Me.LBCommentLine)
        Me.Controls.Add(Me.CommentLine)
        Me.Controls.Add(Me.LBShowPhone)
        Me.Controls.Add(Me.ShowPhone)
        Me.Controls.Add(Me.LBCityWebsite6)
        Me.Controls.Add(Me.CityWebsite6)
        Me.Controls.Add(Me.LBWebsite6)
        Me.Controls.Add(Me.Website6)
        Me.Controls.Add(Me.LBCityPhone6)
        Me.Controls.Add(Me.CityPhone6)
        Me.Controls.Add(Me.LBemail6)
        Me.Controls.Add(Me.email6)
        Me.Controls.Add(Me.LBPhone6)
        Me.Controls.Add(Me.Phone6)
        Me.Controls.Add(Me.LBTitle6)
        Me.Controls.Add(Me.Title6)
        Me.Controls.Add(Me.LBName6)
        Me.Controls.Add(Me.Name6)
        Me.Controls.Add(Me.LBGuest6)
        Me.Controls.Add(Me.LBCityWebsite5)
        Me.Controls.Add(Me.CityWebsite5)
        Me.Controls.Add(Me.LBWebsite5)
        Me.Controls.Add(Me.Website5)
        Me.Controls.Add(Me.LBCityPhone5)
        Me.Controls.Add(Me.CityPhone5)
        Me.Controls.Add(Me.LBemail5)
        Me.Controls.Add(Me.email5)
        Me.Controls.Add(Me.LBPhone5)
        Me.Controls.Add(Me.Phone5)
        Me.Controls.Add(Me.LBTitle5)
        Me.Controls.Add(Me.Title5)
        Me.Controls.Add(Me.LBName5)
        Me.Controls.Add(Me.Name5)
        Me.Controls.Add(Me.LBGuest5)
        Me.Controls.Add(Me.LBCityWebsite4)
        Me.Controls.Add(Me.CityWebsite4)
        Me.Controls.Add(Me.LBWebsite4)
        Me.Controls.Add(Me.Website4)
        Me.Controls.Add(Me.LBCityPhone4)
        Me.Controls.Add(Me.CityPhone4)
        Me.Controls.Add(Me.LBemail4)
        Me.Controls.Add(Me.email4)
        Me.Controls.Add(Me.LBPhone4)
        Me.Controls.Add(Me.Phone4)
        Me.Controls.Add(Me.LBTitle4)
        Me.Controls.Add(Me.Title4)
        Me.Controls.Add(Me.LBName4)
        Me.Controls.Add(Me.Name4)
        Me.Controls.Add(Me.LBGuest4)
        Me.Controls.Add(Me.LBCityWebsite3)
        Me.Controls.Add(Me.CityWebsite3)
        Me.Controls.Add(Me.LBWebsite3)
        Me.Controls.Add(Me.Website3)
        Me.Controls.Add(Me.LBCityPhone3)
        Me.Controls.Add(Me.CityPhone3)
        Me.Controls.Add(Me.LBemail3)
        Me.Controls.Add(Me.email3)
        Me.Controls.Add(Me.LBPhone3)
        Me.Controls.Add(Me.Phone3)
        Me.Controls.Add(Me.LBTitle3)
        Me.Controls.Add(Me.Title3)
        Me.Controls.Add(Me.LBName3)
        Me.Controls.Add(Me.Name3)
        Me.Controls.Add(Me.LBGuest3)
        Me.Controls.Add(Me.LBCityWebsite2)
        Me.Controls.Add(Me.CityWebsite2)
        Me.Controls.Add(Me.LBWebsite2)
        Me.Controls.Add(Me.Website2)
        Me.Controls.Add(Me.LBCityPhone2)
        Me.Controls.Add(Me.CityPhone2)
        Me.Controls.Add(Me.LBemail2)
        Me.Controls.Add(Me.email2)
        Me.Controls.Add(Me.LBPhone2)
        Me.Controls.Add(Me.Phone2)
        Me.Controls.Add(Me.LBTitle2)
        Me.Controls.Add(Me.Title2)
        Me.Controls.Add(Me.LBName2)
        Me.Controls.Add(Me.Name2)
        Me.Controls.Add(Me.LBGuest2)
        Me.Controls.Add(Me.LBCityWebsite1)
        Me.Controls.Add(Me.CityWebsite1)
        Me.Controls.Add(Me.LBWebsite1)
        Me.Controls.Add(Me.Website1)
        Me.Controls.Add(Me.LBCityPhone1)
        Me.Controls.Add(Me.CityPhone1)
        Me.Controls.Add(Me.LBemail1)
        Me.Controls.Add(Me.email1)
        Me.Controls.Add(Me.LBPhone1)
        Me.Controls.Add(Me.Phone1)
        Me.Controls.Add(Me.LBTitle1)
        Me.Controls.Add(Me.Title1)
        Me.Controls.Add(Me.LBName1)
        Me.Controls.Add(Me.Name1)
        Me.Controls.Add(Me.LBGuest1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.Text = "Graphics Builder   V1.53"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LBGuest1 As Label
    Friend WithEvents Name1 As TextBox
    Friend WithEvents LBName1 As Label
    Friend WithEvents LBTitle1 As Label
    Friend WithEvents Title1 As TextBox
    Friend WithEvents LBPhone1 As Label
    Friend WithEvents Phone1 As TextBox
    Friend WithEvents LBemail1 As Label
    Friend WithEvents email1 As TextBox
    Friend WithEvents LBCityPhone1 As Label
    Friend WithEvents CityPhone1 As TextBox
    Friend WithEvents LBWebsite1 As Label
    Friend WithEvents Website1 As TextBox
    Friend WithEvents LBCityWebsite1 As Label
    Friend WithEvents CityWebsite1 As TextBox
    Friend WithEvents LBCityWebsite2 As Label
    Friend WithEvents CityWebsite2 As TextBox
    Friend WithEvents LBWebsite2 As Label
    Friend WithEvents Website2 As TextBox
    Friend WithEvents LBCityPhone2 As Label
    Friend WithEvents CityPhone2 As TextBox
    Friend WithEvents LBemail2 As Label
    Friend WithEvents email2 As TextBox
    Friend WithEvents LBPhone2 As Label
    Friend WithEvents Phone2 As TextBox
    Friend WithEvents LBTitle2 As Label
    Friend WithEvents Title2 As TextBox
    Friend WithEvents LBName2 As Label
    Friend WithEvents Name2 As TextBox
    Friend WithEvents LBGuest2 As Label
    Friend WithEvents LBCityWebsite3 As Label
    Friend WithEvents CityWebsite3 As TextBox
    Friend WithEvents LBWebsite3 As Label
    Friend WithEvents Website3 As TextBox
    Friend WithEvents LBCityPhone3 As Label
    Friend WithEvents CityPhone3 As TextBox
    Friend WithEvents LBemail3 As Label
    Friend WithEvents email3 As TextBox
    Friend WithEvents LBPhone3 As Label
    Friend WithEvents Phone3 As TextBox
    Friend WithEvents LBTitle3 As Label
    Friend WithEvents Title3 As TextBox
    Friend WithEvents LBName3 As Label
    Friend WithEvents Name3 As TextBox
    Friend WithEvents LBGuest3 As Label
    Friend WithEvents LBCityWebsite6 As Label
    Friend WithEvents CityWebsite6 As TextBox
    Friend WithEvents LBWebsite6 As Label
    Friend WithEvents Website6 As TextBox
    Friend WithEvents LBCityPhone6 As Label
    Friend WithEvents CityPhone6 As TextBox
    Friend WithEvents LBemail6 As Label
    Friend WithEvents email6 As TextBox
    Friend WithEvents LBPhone6 As Label
    Friend WithEvents Phone6 As TextBox
    Friend WithEvents LBTitle6 As Label
    Friend WithEvents Title6 As TextBox
    Friend WithEvents LBName6 As Label
    Friend WithEvents Name6 As TextBox
    Friend WithEvents LBGuest6 As Label
    Friend WithEvents LBCityWebsite5 As Label
    Friend WithEvents CityWebsite5 As TextBox
    Friend WithEvents LBWebsite5 As Label
    Friend WithEvents Website5 As TextBox
    Friend WithEvents LBCityPhone5 As Label
    Friend WithEvents CityPhone5 As TextBox
    Friend WithEvents LBemail5 As Label
    Friend WithEvents email5 As TextBox
    Friend WithEvents LBPhone5 As Label
    Friend WithEvents Phone5 As TextBox
    Friend WithEvents LBTitle5 As Label
    Friend WithEvents Title5 As TextBox
    Friend WithEvents LBName5 As Label
    Friend WithEvents Name5 As TextBox
    Friend WithEvents LBGuest5 As Label
    Friend WithEvents LBCityWebsite4 As Label
    Friend WithEvents CityWebsite4 As TextBox
    Friend WithEvents LBWebsite4 As Label
    Friend WithEvents Website4 As TextBox
    Friend WithEvents LBCityPhone4 As Label
    Friend WithEvents CityPhone4 As TextBox
    Friend WithEvents LBemail4 As Label
    Friend WithEvents email4 As TextBox
    Friend WithEvents LBPhone4 As Label
    Friend WithEvents Phone4 As TextBox
    Friend WithEvents LBTitle4 As Label
    Friend WithEvents Title4 As TextBox
    Friend WithEvents LBName4 As Label
    Friend WithEvents Name4 As TextBox
    Friend WithEvents LBGuest4 As Label
    Friend WithEvents LBRecordDate As Label
    Friend WithEvents RecordDate As TextBox
    Friend WithEvents LBShowYouTube As Label
    Friend WithEvents ShowYouTube As TextBox
    Friend WithEvents LBCommentLine As Label
    Friend WithEvents CommentLine As TextBox
    Friend WithEvents LBShowPhone As Label
    Friend WithEvents ShowPhone As TextBox
    Friend WithEvents LBHost1 As Label
    Friend WithEvents Host1 As TextBox
    Friend WithEvents LBProducer1 As Label
    Friend WithEvents Producer1 As TextBox
    Friend WithEvents LBDirector2 As Label
    Friend WithEvents Director2 As TextBox
    Friend WithEvents LBDirector1 As Label
    Friend WithEvents Director1 As TextBox
    Friend WithEvents LBGraphics2 As Label
    Friend WithEvents Graphics2 As TextBox
    Friend WithEvents LBGraphics1 As Label
    Friend WithEvents Graphics1 As TextBox
    Friend WithEvents LBAudio2 As Label
    Friend WithEvents Audio2 As TextBox
    Friend WithEvents LBAudio1 As Label
    Friend WithEvents Audio1 As TextBox
    Friend WithEvents LBCamera3 As Label
    Friend WithEvents Camera3 As TextBox
    Friend WithEvents LBCamera2 As Label
    Friend WithEvents Camera2 As TextBox
    Friend WithEvents LBCamera1 As Label
    Friend WithEvents Camera1 As TextBox
    Friend WithEvents ButtonSave As Button
    Friend WithEvents DB1 As RadioButton
    Friend WithEvents DB2 As RadioButton
    Friend WithEvents DB3 As RadioButton
    Friend WithEvents DB4 As RadioButton
    Friend WithEvents DB5 As RadioButton
    Friend WithEvents DB6 As RadioButton
    Friend WithEvents DBbox As ComboBox
    Friend WithEvents DBload As Button
    Friend WithEvents Clear As Button
    Friend WithEvents SaveDB As Button
    Friend WithEvents LBShowTitle As Label
    Friend WithEvents ShowTitle As TextBox
    Friend WithEvents LBGuestDB As Label
    Friend WithEvents CDBSave As Button
    Friend WithEvents CDBload As Button
    Friend WithEvents LBCDB As Label
    Friend WithEvents CDBbox As ComboBox
    Friend WithEvents CDBProducer1 As RadioButton
    Friend WithEvents CDBHost1 As RadioButton
    Friend WithEvents CDBDirector2 As RadioButton
    Friend WithEvents CDBDirector1 As RadioButton
    Friend WithEvents CDBGraphics2 As RadioButton
    Friend WithEvents CDBGraphics1 As RadioButton
    Friend WithEvents CDBCamera3 As RadioButton
    Friend WithEvents CDBCamera2 As RadioButton
    Friend WithEvents CDBCamera1 As RadioButton
    Friend WithEvents CDBAudio2 As RadioButton
    Friend WithEvents CDBAudio1 As RadioButton
    Friend WithEvents TextFileCheckTime As Timer
    Friend WithEvents LBShowTwitter As Label
    Friend WithEvents ShowTwitter As TextBox
    Friend WithEvents LBShowFacebook As Label
    Friend WithEvents ShowFacebook As TextBox
    Friend WithEvents LBShowWebsite As Label
    Friend WithEvents ShowWebsite As TextBox
    Friend WithEvents LBShowName As Label
    Friend WithEvents ShowName As TextBox
    Friend WithEvents LBShowemail As Label
    Friend WithEvents Showemail As TextBox
    Friend WithEvents CDBFloor3 As RadioButton
    Friend WithEvents CDBFloor2 As RadioButton
    Friend WithEvents CDBFloor1 As RadioButton
    Friend WithEvents LBFloor3 As Label
    Friend WithEvents Floor3 As TextBox
    Friend WithEvents LBFloor2 As Label
    Friend WithEvents Floor2 As TextBox
    Friend WithEvents LBFloor1 As Label
    Friend WithEvents Floor1 As TextBox
    Friend WithEvents CDBProducer2 As RadioButton
    Friend WithEvents LBProducer2 As Label
    Friend WithEvents Producer2 As TextBox
    Friend WithEvents CDBHost2 As RadioButton
    Friend WithEvents LBHost2 As Label
    Friend WithEvents Host2 As TextBox
    Friend WithEvents LBTwitter3 As Label
    Friend WithEvents Twitter3 As TextBox
    Friend WithEvents LBFacebook3 As Label
    Friend WithEvents Facebook3 As TextBox
    Friend WithEvents LBTwitter2 As Label
    Friend WithEvents Twitter2 As TextBox
    Friend WithEvents LBFacebook2 As Label
    Friend WithEvents Facebook2 As TextBox
    Friend WithEvents LBTwitter1 As Label
    Friend WithEvents Twitter1 As TextBox
    Friend WithEvents LBFacebook1 As Label
    Friend WithEvents Facebook1 As TextBox
    Friend WithEvents LBTwitter6 As Label
    Friend WithEvents Twitter6 As TextBox
    Friend WithEvents LBFacebook6 As Label
    Friend WithEvents Facebook6 As TextBox
    Friend WithEvents LBTwitter5 As Label
    Friend WithEvents Twitter5 As TextBox
    Friend WithEvents LBFacebook5 As Label
    Friend WithEvents Facebook5 As TextBox
    Friend WithEvents LBTwitter4 As Label
    Friend WithEvents Twitter4 As TextBox
    Friend WithEvents LBFacebook4 As Label
    Friend WithEvents Facebook4 As TextBox
    Friend WithEvents LBYouTube3 As Label
    Friend WithEvents YouTube3 As TextBox
    Friend WithEvents LBYouTube2 As Label
    Friend WithEvents YouTube2 As TextBox
    Friend WithEvents LBYouTube1 As Label
    Friend WithEvents YouTube1 As TextBox
    Friend WithEvents LBYouTube6 As Label
    Friend WithEvents YouTube6 As TextBox
    Friend WithEvents LBYouTube5 As Label
    Friend WithEvents YouTube5 As TextBox
    Friend WithEvents LBYouTube4 As Label
    Friend WithEvents YouTube4 As TextBox
    Friend WithEvents ButtonSettings As Button
    Friend WithEvents LBOfficeemail6 As Label
    Friend WithEvents Officeemail6 As TextBox
    Friend WithEvents LBOfficePhone6 As Label
    Friend WithEvents OfficePhone6 As TextBox
    Friend WithEvents LBOfficeemail5 As Label
    Friend WithEvents Officeemail5 As TextBox
    Friend WithEvents LBOfficePhone5 As Label
    Friend WithEvents OfficePhone5 As TextBox
    Friend WithEvents LBOfficeemail4 As Label
    Friend WithEvents Officeemail4 As TextBox
    Friend WithEvents LBOfficePhone4 As Label
    Friend WithEvents OfficePhone4 As TextBox
    Friend WithEvents LBOfficeemail3 As Label
    Friend WithEvents Officeemail3 As TextBox
    Friend WithEvents LBOfficePhone3 As Label
    Friend WithEvents OfficePhone3 As TextBox
    Friend WithEvents LBOfficeemail2 As Label
    Friend WithEvents Officeemail2 As TextBox
    Friend WithEvents LBOfficePhone2 As Label
    Friend WithEvents OfficePhone2 As TextBox
    Friend WithEvents LBOfficeemail1 As Label
    Friend WithEvents Officeemail1 As TextBox
    Friend WithEvents LBOfficePhone1 As Label
    Friend WithEvents OfficePhone1 As TextBox
End Class
